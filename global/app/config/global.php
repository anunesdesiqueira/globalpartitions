<?php

    return array(
        'downlaods' => array(
            'products' => array(
                'path' => '/public/uploads/acessorios/downloads/',
                'public_path' => '/uploads/acessorios/downloads/',
            ),
        ),
        'images' => array(
            'carousel' => array(
                'path' => '/public/uploads/carousel/',
                'public_path' => '/uploads/carousel/',
                'thumbnails' => array(
                    'default' 	=> array(1920, 732),
                ),
            ),
            'partitions' => array(
                'background' => array(
                    'path' => '/public/uploads/divisorias/background/',
                    'public_path' => '/uploads/divisorias/background/',
                    'thumbnails' => array(),
                ),
                'gallery' => array(
                    'path' => '/public/uploads/divisorias/galeria/',
                    'public_path' => '/uploads/divisorias/galeria/',
                    'thumbnails' => array(
                        'small' 	=> array(128, 81),
                        'medium' 	=> array(159, 101),
                        'main'      => array(170, 108),
                        'default' 	=> array(238, 150),
                    ),
                ),
            ),
            'accessories' => array(
                'category' => array(
                    'path' => '/public/uploads/acessorios/categorias/',
                    'public_path' => '/uploads/acessorios/categorias/',
                    'thumbnails' => array(
                        'small' 	=> array(128, 81),
                        'medium' 	=> array(159, 101),
                        'main'      => array(170, 108),
                        'default' 	=> array(238, 150),
                    ),
                ),
                'product' => array(
                    'path' => '/public/uploads/acessorios/produtos/',
                    'public_path' => '/uploads/acessorios/produtos/',
                    'thumbnails' => array(
                        'small' 	=> array(153, 153),
                        'medium' 	=> array(190, 190),
                        'main'      => array(203, 203),
                        'default' 	=> array(284, 284),
                    ),
                ),
            ),
        ),
    );