<?php

    $colorPath = 'uploads/simulador/cores/';

    return array(
        'colors' => array(
            array(
                'hex' => '#cbb9af',
                'cod'  => 'L 003',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '003.jpg',
            ),

            array(
                'hex' => '#7a85c5',
                'cod'  => 'L 004',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '004.jpg',
            ),

            array(
                'hex' => '#9f9795',
                'cod'  => 'L 010',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '010.jpg',
            ),

            array(
                'hex' => '#b1bf50',
                'cod'  => 'L 011',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '011.jpg',
            ),

            array(
                'hex' => '#243386',
                'cod'  => 'L 012',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '012.jpg',
            ),

            array(
                'hex' => '#676765',
                'cod'  => 'L 013',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '013.jpg',
            ),

            array(
                'hex' => '#262a73',
                'cod'  => 'L 019',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '019.jpg',
            ),

            array(
                'hex' => '#e5d8d0',
                'cod'  => 'L 022',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '022.jpg',
            ),

            array(
                'hex' => '#95342d',
                'cod'  => 'L 101',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '101.jpg',
            ),

            array(
                'hex' => '#b77845',
                'cod'  => 'L 103',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '103.jpg',
            ),

            array(
                'hex' => '#d9cf91',
                'cod'  => 'L 105',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '105.jpg',
            ),

            array(
                'hex' => '#4f8178',
                'cod'  => 'L 114',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '114.jpg',
            ),

            array(
                'hex' => '#e6dfd7',
                'cod'  => 'L 106',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '106.jpg',
            ),

            array(
                'hex' => '#bcb098',
                'cod'  => 'L 108',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '108.jpg',
            ),

            array(
                'hex' => '#93aa96',
                'cod'  => 'L 110',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '110.jpg',
            ),

            array(
                'hex' => '#bab99a',
                'cod'  => 'L 111',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '111.jpg',
            ),

            array(
                'hex' => '#e8dccc',
                'cod'  => 'L 112',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '112.jpg',
            ),

            array(
                'hex' => '#30562f',
                'cod'  => 'L 113',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '113.jpg',
            ),

            array(
                'hex' => '#2d4c47',
                'cod'  => 'L 114',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '114.jpg',
            ),

            array(
                'hex' => '#a9bccd',
                'cod'  => 'L 115',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '115.jpg',
            ),

            array(
                'hex' => '#777b80',
                'cod'  => 'L 151',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '151.jpg',
            ),

            array(
                'hex' => '#275785',
                'cod'  => 'L 117',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '117.jpg',
            ),

            array(
                'hex' => '#958e88',
                'cod'  => 'L 118',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '118.jpg',
            ),

            array(
                'hex' => '#bab5af',
                'cod'  => 'L 119',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '119.jpg',
            ),

            array(
                'hex' => '#e1d8cf',
                'cod'  => 'L 120',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '120.jpg',
            ),

            array(
                'hex' => '#2d2a25',
                'cod'  => 'L 121',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '121.jpg',
            ),

            array(
                'hex' => '#181718',
                'cod'  => 'L 121',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '121.jpg',
            ),

            array(
                'hex' => '#57849b',
                'cod'  => 'L 122',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '122.jpg',
            ),

            array(
                'hex' => '#5e952f',
                'cod'  => 'L 131',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '131.jpg',
            ),

            array(
                'hex' => '#a59284',
                'cod'  => 'L 132',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '132.jpg',
            ),

            array(
                'hex' => '#592527',
                'cod'  => 'L 138',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '138.jpg',
            ),

            array(
                'hex' => '#a19798',
                'cod'  => 'L 139',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '139.jpg',
            ),

            array(
                'hex' => '#e9dbd0',
                'cod'  => 'L 141',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '141.jpg',
            ),

            array(
                'hex' => '#ccb484',
                'cod'  => 'L 142',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '142.jpg',
            ),

            array(
                'hex' => '#60352f',
                'cod'  => 'L 144',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '144.jpg',
            ),

            array(
                'hex' => '#9e6359',
                'cod'  => 'L 148',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '148.jpg',
            ),

            array(
                'hex' => '#383330',
                'cod'  => 'L 147',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '147.jpg',
            ),

            array(
                'hex' => '#c3a694',
                'cod'  => 'L 148',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '148.jpg',
            ),

            array(
                'hex' => '#4f5668',
                'cod'  => 'L 150',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '150.jpg',
            ),

            array(
                'hex' => '#83807b',
                'cod'  => 'L 151',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '151.jpg',
            ),

            array(
                'hex' => '#8f8f8f',
                'cod'  => 'L 155',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '155.jpg',
            ),

            array(
                'hex' => '#8e8d7b',
                'cod'  => 'L 158',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '158.jpg',
            ),

            array(
                'hex' => '#aba6a3',
                'cod'  => 'L 166',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '166.jpg',
            ),

            array(
                'hex' => '#9db3c8',
                'cod'  => 'L 168',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '168.jpg',
            ),

            array(
                'hex' => '#c0af9f',
                'cod'  => 'L 173',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '173.jpg',
            ),

            array(
                'hex' => '#222133',
                'cod'  => 'L 178',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '178.jpg',
            ),

            array(
                'hex' => '#bfc6be',
                'cod'  => 'L 182',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '182.jpg',
            ),

            array(
                'hex' => '#e3dcd6',
                'cod'  => 'L 190',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '190.jpg',
            ),

            array(
                'hex' => '#e0dcdb',
                'cod'  => 'L 515',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '515.jpg',
            ),

            array(
                'hex' => '#c9ac46',
                'cod'  => 'L 523',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '523.jpg',
            ),

            array(
                'hex' => '#d88e13',
                'cod'  => 'L 551',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '551.jpg',
            ),

            array(
                'hex' => '#482454',
                'cod'  => 'L 552',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '552.jpg',
            ),

            array(
                'hex' => '#9b2d83',
                'cod'  => 'L 555',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '555.jpg',
            ),

            array(
                'hex' => '#686657',
                'cod'  => 'L 560',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '560.jpg',
            ),

            array(
                'hex' => '#b9ae9a',
                'cod'  => 'L 562',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '562.jpg',
            ),

            array(
                'hex' => '#bea8ce',
                'cod'  => 'L 564',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '564.jpg',
            ),

            array(
                'hex' => '#a36095',
                'cod'  => 'L 566',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . '566.jpg',
            ),

            array(
                'hex' => '#807478',
                'cod'  => 'IN 80',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . 'in80.jpg',
            ),
        ),

        'specialColors' => array(
            array(
                'cod'  => 'M 450',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . 'm450.jpg',
                'background' => 'assets/images/special-default-01.png',
            ),
            array(
                'cod'  => 'M 807',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . 'm807.jpg',
                'background' => 'assets/images/special-default-02.png',
            ),
            array(
                'cod'  => 'M 849.jpg',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . 'm849.jpg',
                'background' => 'assets/images/special-default-03.png',
            ),
            array(
                'cod'  => 'M 819',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . 'm814.jpg',
                'background' => 'assets/images/special-default-04.png',
            ),
            array(
                'cod'  => 'M 819',
                'name'   => 'Lorem ipsum dolor',
                'image' => $colorPath . 'm819.jpg',
                'background' => 'assets/images/special-default-05.png',
            ),
        ),
    );