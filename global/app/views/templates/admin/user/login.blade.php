<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Admin Global Sanitários | Login</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <!-- bootstrap 3.0.2 -->
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{ asset('assets/css/admin.css') }}" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
          <script>window.html5 || document.write('<script src="assets/js/vendor/html5shiv.js"><\/script>')</script>

          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">

        <div class="form-box" id="login-box">
            <div class="header">Login</div>
            {{ Form::open(array('route' => 'admin.login', 'method' => 'post')) }}
                <div class="body bg-gray">

                    @if(Session::has('error_message'))
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ Session::get('error_message') }}
                        </div>
                    @endif

                    <div class="form-group">
                        {{ Form::text('email', '', array('class' => 'form-control', 'placeholder' => 'Email')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Senha')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::checkbox('remember_me') }} Mantenha-me conectado
                    </div>
                </div>
                <div class="footer">
                    {{ Form::submit('Entrar', array('class' => 'btn bg-olive btn-block')) }}
                    <p><a href="{{ route('admin.forgot') }}">Esqueci minha senha</a></p>
                </div>
            {{ Form::close() }}
        </div>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        {{ HTML::script('assets/js/vendor/bootstrap.min.js') }}

    </body>
</html>