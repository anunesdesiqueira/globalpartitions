@extends('templates.admin.layouts.master')

@section('content')
    <div class="col-md-12">

        @if($errors->has())
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($errors->all() as $message)
                    {{ $message }}<br>
                @endforeach
            </div>
        @endif

        @if(isset($alert))
            <div class="alert {{ array_key_exists('success', $alert) ? 'alert-success' : 'alert-warning' }} alert-dismissable">
                <i class="fa {{ array_key_exists('success', $alert) ? 'fa-check' : 'fa-warning' }}"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($alert as $message)
                    {{ $message }}<br>
                @endforeach
            </div>
        @endif

        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Editar Banner</h3>
                <div class="box-tools">
                    <a class="btn btn-default" href="{{ route('admin.carousel') }}"><i class="fa fa-bars"></i> Listar Banners</a>
                </div>
            </div><!-- /.box-header -->
            <!-- form start -->
            {{ Form::open(array('route' => array('admin.carousel.edit', $banner->id), 'method' => 'post', 'enctype' => 'multipart/form-data')) }}
                <div class="box-body">
                    <div class="form-group">
                        <label for="title">Titulo</label>
                        {{ Form::text('title', Input::old('title', $banner->title), array('class' => 'form-control', 'id' => 'title', 'placeholder' => 'Titulo do banner')) }}
                    </div>
                    <div class="form-group">
                        <label for="name">Descrição</label>
                        {{ Form::text('description', Input::old('description', $banner->description), array('class' => 'form-control', 'id' => 'description', 'placeholder' => 'Descrição do banner')) }}
                    </div>
                    <div class="form-group">
                        <label for="order">Ordem</label>
                        {{ Form::text('order', Input::old('order', $banner->order), array('class' => 'form-control', 'id' => 'order', 'placeholder' => 'Insira a posição', 'style' => 'width: 150px;')) }}
                    </div>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 238px; height: 150px;">
                            <img src="{{ $banner->image['original'] }}" alt="238x150" style="height: 100%; width: 100%; display: block;">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Selecionar</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="image">
                            </span>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                        </div>
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
            {{ Form::close() }}
        </div><!-- /.box -->
    </div>
@stop