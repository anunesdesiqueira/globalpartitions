@extends('templates.admin.layouts.master')

@section('content')
    <div class="col-md-12">

        @if($errors->has())
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($errors->all() as $message)
                    {{ $message }}<br>
                @endforeach
            </div>
        @endif

        @if(isset($alert))
            <div class="alert {{ array_key_exists('success', $alert) ? 'alert-success' : 'alert-warning' }} alert-dismissable">
                <i class="fa {{ array_key_exists('success', $alert) ? 'fa-check' : 'fa-warning' }}"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($alert as $message)
                    {{ $message }}<br>
                @endforeach
            </div>
        @endif

        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Cadastrar Nova Divisória</h3>
                <div class="box-tools">
                    <a class="btn btn-default" href="{{ route('admin.partitions.index') }}"><i class="fa fa-bars"></i> Listar Divisórias</a>
                </div>
            </div><!-- /.box-header -->
            <!-- form start -->
            {{ Form::open(array('route' => 'admin.partitions.create', 'method' => 'post', 'enctype' => 'multipart/form-data')) }}
                <div class="box-body">
                    <div class="form-group">
                        <label for="title">Titulo</label>
                        {{ Form::text('title', Input::old('title'), array('class' => 'form-control', 'id' => 'title', 'placeholder' => 'Titulo da divisória')) }}
                    </div>
                    <div class="form-group">
                        <label for="permalink">Link</label>
                        <div class="input-group">
                            <div class="input-group-addon">http://www.globalsanitarios.com.br/produtos/</div>
                            {{ Form::text('permalink', Input::old('permalink'), array('class' => 'form-control', 'id' => 'permalink', 'placeholder' => 'Inserir url amigável')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="content">Descrição</label>
                        {{ Form::textarea('content', Input::old('content'), array('class' => 'textarea', 'id' => 'content', 'placeholder' => 'Escreva a descrição', 'style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;')) }}
                    </div>
                    <div class="form-group">
                        <label for="order">Ordem</label>
                        {{ Form::text('order', Input::old('order'), array('class' => 'form-control', 'id' => 'order', 'placeholder' => 'Insira a posição', 'style' => 'width: 150px;')) }}
                    </div>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail">
                            <img src="" style="height: 100%; width: 100%; display: block;">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail"></div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Selecionar</span>
                                <span class="fileinput-exists">Change</span>
                                {{ Form::file('background') }}
                            </span>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                        </div>
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
            {{ Form::close() }}
        </div><!-- /.box -->
    </div>
@stop

@section('customjs')
    <!-- Bootstrap WYSIHTML5 -->
    {{ HTML::script('assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}
    <script type="text/javascript">
        $(function() {
            'use strict';

            //bootstrap WYSIHTML5 - text editor
            $(".textarea").wysihtml5({
                "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": true, //Italics, bold, etc. Default true
                "lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": false //Button to change color of font
            });

        });

    </script>
@stop