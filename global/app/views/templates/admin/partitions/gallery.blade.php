@extends('templates.admin.layouts.master')

@section('content')
    <div class="col-md-12">

        @if($errors->has())
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($errors->all() as $message)
                    {{ $message }}<br>
                @endforeach
            </div>
        @endif

        @if(isset($alert))
            <div class="alert {{ array_key_exists('success', $alert) ? 'alert-success' : 'alert-warning' }} alert-dismissable">
                <i class="fa {{ array_key_exists('success', $alert) ? 'fa-check' : 'fa-warning' }}"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($alert as $message)
                    {{ $message }}<br>
                @endforeach
            </div>
        @endif

        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Gallery Divisória - {{ $partition->title }}</h3>
                <div class="box-tools">
                    <a class="btn btn-default" href="{{ route('admin.partitions.index') }}"><i class="fa fa-bars"></i> Listar Divisória</a>
                </div>
            </div><!-- /.box-header -->
            <!-- form start -->
            {{ Form::open(array('route' => array('admin.partitions.gallery', $partition->id), 'method' => 'post', 'enctype' => 'multipart/form-data')) }}
                <div class="box-body">
                    <div class="form-group">
                        <input type="file" name="image[]" multiple>
                        <p class="help-block">Adicione as imagens para galeria.</p>
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
            {{ Form::close() }}
        </div><!-- /.box -->

        @if(count($partition->gallery) > 0)
         <div class="box box-primary">
            <div id="gallery-images" class="box-body">
                <div class="row">
                    @foreach($partition->gallery as $image)
                        <div class="col-md-3">
                            <div class="thumbnail">
                                <img src="{{ $image->image['main'] }}">
                                <div class="caption" style="text-align: center">
                                    <button class="btn btn-primary" data-image-url="{{ route('admin.partitions.gallery.destroy', $image->id) }}" role="button">Remover</button>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
         </div>
        @endif
    </div>
@stop

@section('customjs')
    <script type="text/javascript">
        $(function() {
            'use strict';

            $('#gallery-images button').on('click', function(e) {
                e.preventDefault();

                var $this = $(this);
                var urlImage = $this.attr('data-image-url');

                $this.parent().parent().parent().remove();

                $.ajax({
                    url : urlImage,
                    type: "DELETE",
                    success: function(data, textStatus, jqXHR) {}
                });

            });
        });
    </script>
@stop