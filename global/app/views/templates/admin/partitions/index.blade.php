@extends('templates.admin.layouts.master')

@section('content')
    <div class="col-xs-12">

        @if(Session::has('error_message'))
           <div class="alert alert-warning alert-dismissable">
               <i class="fa fa-warning"></i>
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               {{ Session::get('error_message') }}
           </div>
        @endif

        @if(Session::has('success_message'))
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('success_message') }}
            </div>
        @endif

        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Listar Divisórias</h3>
                {{--<div class="box-tools">
                    <button class="btn btn-default"><a href="{{ route('admin.accessories.products.create') }}"><i class="fa fa-plus"></i> Novo Produto</a></button>
                </div> --}}
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="datatable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Titulo</th>
                            <th>Link</th>
                            <th>Descrição</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($allPartitions as $partition)
                        <tr>
                            <td>{{ $partition->title }}</td>
                            <td>{{ $partition->permalink }}</td>
                            <td>{{ $partition->content }}</td>
                            <td>
                                <a href="{{ route('admin.partitions.gallery', array('id' => $partition->id)) }}" class="btn btn-success btn-sm pull-left"><i class="fa fa-picture-o"></i> Galeria de Fotos</a><br><br>
                                <a href="{{ route('admin.partitions.edit', array('id' => $partition->id)) }}" class="btn btn-info btn-sm pull-left"><i class="fa fa-edit"></i> Editar</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
@stop

@section('customjs')
    <script type="text/javascript">
        $(function() {
            $('#datatable').dataTable({
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": false,
                "bSort": false,
                "bInfo": false,
                "bAutoWidth": false
            });
        });
    </script>
@stop