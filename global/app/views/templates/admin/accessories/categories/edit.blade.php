@extends('templates.admin.layouts.master')

@section('content')
    <div class="col-md-12">

        @if($errors->has())
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($errors->all() as $message)
                    {{ $message }}<br>
                @endforeach
            </div>
        @endif

        @if(isset($alert))
            <div class="alert {{ array_key_exists('success', $alert) ? 'alert-success' : 'alert-warning' }} alert-dismissable">
                <i class="fa {{ array_key_exists('success', $alert) ? 'fa-check' : 'fa-warning' }}"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($alert as $message)
                    {{ $message }}<br>
                @endforeach
            </div>
        @endif

        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Editar Categoria</h3>
                <div class="box-tools">
                    <a class="btn btn-default" href="{{ route('admin.accessories.categories.index') }}"><i class="fa fa-bars"></i> Listar Categorias</a>
                </div>
            </div><!-- /.box-header -->
            <!-- form start -->
            {{ Form::open(array('route' => array('admin.accessories.categories.edit', $category->id), 'method' => 'post', 'enctype' => 'multipart/form-data')) }}
                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Categoria</label>
                        {{ Form::text('name', Input::old('name', $category->name), array('class' => 'form-control', 'id' => 'name', 'placeholder' => 'Nome da Categoria')) }}
                    </div>
                    <div class="form-group">
                        <label for="permalink">Link</label>
                        <div class="input-group">
                            <div class="input-group-addon">http://www.globalsanitarios.com.br/acessorios/</div>
                            {{ Form::text('permalink', Input::old('permalink', $category->permalink), array('class' => 'form-control', 'id' => 'permalink', 'placeholder' => 'Inserir url amigável')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="order">Ordem</label>
                        {{ Form::text('order', Input::old('order', $category->order), array('class' => 'form-control', 'id' => 'order', 'placeholder' => 'Insira a posição', 'style' => 'width: 150px;')) }}
                    </div>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 238px; height: 150px;">
                            <img src="{{ $category->image['original'] }}" alt="238x150" style="height: 100%; width: 100%; display: block;">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Selecionar</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="image">
                            </span>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                        </div>
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
            {{ Form::close() }}
        </div><!-- /.box -->
    </div>
@stop