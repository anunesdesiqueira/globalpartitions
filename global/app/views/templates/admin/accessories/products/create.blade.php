@extends('templates.admin.layouts.master')

@section('content')
    <div class="col-md-12">

        @if($errors->has())
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($errors->all() as $message)
                    {{ $message }}<br>
                @endforeach
            </div>
        @endif

        @if(isset($alert))
            <div class="alert {{ array_key_exists('success', $alert) ? 'alert-success' : 'alert-warning' }} alert-dismissable">
                <i class="fa {{ array_key_exists('success', $alert) ? 'fa-check' : 'fa-warning' }}"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($alert as $message)
                    {{ $message }}<br>
                @endforeach
            </div>
        @endif

        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Cadastrar Novo Produto</h3>
                <div class="box-tools">
                    <a class="btn btn-default" href="{{ route('admin.accessories.products.index') }}"><i class="fa fa-bars"></i> Listar Produtos</a>
                </div>
            </div><!-- /.box-header -->
            <div class="content">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Principal</a></li>
                        <li><a href="#tab_2" data-toggle="tab">Imagens</a></li>
                        <li><a href="#tab_3" data-toggle="tab">Downloads</a></li>
                    </ul>
                    <!-- form start -->
                    {{ Form::open(array('route' => 'admin.accessories.products.create', 'method' => 'post', 'enctype' => 'multipart/form-data')) }}
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">

                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="category_id">Categorias</label>
                                        {{ Form::select('category_id', $listsCategories, Input::old('category_id') , array('id' => 'category_id', 'class' => 'form-control')) }}
                                    </div>
                                    <div class="form-group">
                                        <label for="cod">Código</label>
                                        {{ Form::text('cod', Input::old('cod'), array('id' => 'cod', 'class' => 'form-control', 'style' => 'width: 150px', 'placeholder' => 'Código do produto')) }}
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Nome</label>
                                        {{ Form::text('name', Input::old('name'), array('id' => 'name', 'class' => 'form-control', 'placeholder' => 'Insira o nome')) }}
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Descrição</label>
                                        {{ Form::textarea('description', Input::old('description'), array('class' => 'textarea', 'placeholder' => 'Escreva aa descrição', 'style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;')) }}
                                    </div>
                                    <div class="form-group">
                                        <label for="dimensions">Dimensões</label>
                                        {{ Form::text('dimensions', Input::old('dimensions'), array('id' => 'dimensions', 'class' => 'form-control', 'placeholder' => 'Insira as dimensões do produto')) }}
                                    </div>
                                    <div class="form-group">
                                        <label for="order">Ordem</label>
                                        {{ Form::text('order', Input::old('order'), array('id' => 'order', 'class' => 'form-control', 'style' => 'width: 150px', 'placeholder' => 'Insira a posição')) }}
                                    </div>
                                </div><!-- /.box-body -->

                            </div><!-- /.tab-pane -->

                            <div class="tab-pane" id="tab_2">
                                <div class="box-body">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 284px; height: 284px;">
                                            <img src="" alt="238x150" style="height: 100%; width: 100%; display: block;">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                        <div>
                                            <span class="btn btn-default btn-file">
                                                <span class="fileinput-new">Selecionar</span>
                                                <span class="fileinput-exists">Change</span>
                                                {{ Form::file('image') }}
                                            </span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.tab-pane -->

                            <div class="tab-pane" id="tab_3">
                                <div class="box-body">
                                    <div class="form-group">
                                        <input type="file" name="files[]" multiple>
                                        <p class="help-block">Adicione os arquivo(s) para download.</p>
                                    </div>
                                </div>
                            </div><!-- /.tab-pane -->

                        </div><!-- /.tab-content -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Salvar</button>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div><!-- /.box -->
    </div>
@stop

@section('customjs')
    <!-- Bootstrap WYSIHTML5 -->
    {{ HTML::script('assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}
    <script type="text/javascript">
        $(function() {
            'use strict';

            //bootstrap WYSIHTML5 - text editor
            $(".textarea").wysihtml5({
                "font-styles": false, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": false, //Italics, bold, etc. Default true
                "lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": false //Button to change color of font
            });

        });

    </script>
@stop