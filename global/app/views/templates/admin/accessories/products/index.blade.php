@extends('templates.admin.layouts.master')

@section('content')
    <div class="col-xs-12">

        @if(Session::has('error_message'))
           <div class="alert alert-warning alert-dismissable">
               <i class="fa fa-warning"></i>
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               {{ Session::get('error_message') }}
           </div>
        @endif

        @if(Session::has('success_message'))
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ Session::get('success_message') }}
            </div>
        @endif

        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Listar Produtos</h3>
                <div class="box-tools">
                    <a class="btn btn-default" href="{{ route('admin.accessories.products.create') }}"><i class="fa fa-plus"></i> Novo Produto</a>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="datatable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Categoria</th>
                            <th>Código</th>
                            <th>Nome</th>
                            <th>Ordem</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($allProducts as $product)
                        <tr>
                            <td>{{ $product->categoy_name }}</td>
                            <td>{{ $product->product_cod }}</td>
                            <td>{{ $product->product_name }}</td>
                            <td>{{ $product->product_order }}</td>
                            <td>
                                <a href="{{ route('admin.accessories.products.edit', array('id' => $product->product_id)) }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> Editar</a>
                                <a href="{{ route('admin.accessories.products.destroy', array('id' => $product->product_id)) }}" class="btn btn-danger btn-sm"><i class="fa fa-times-circle-o"></i> Excluir</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Categoria</th>
                            <th>Código</th>
                            <th>Nome</th>
                            <th>Ordem</th>
                            <th>Ações</th>
                        </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
@stop

@section('customjs')
    <script type="text/javascript">
        $(function() {
            $('#datatable').dataTable({
                "bPaginate": true,
                "bLengthChange": false,
                "bFilter": false,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false
            });
        });
    </script>
@stop