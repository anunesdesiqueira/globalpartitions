<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Admin Global Sanitários | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <!-- bootstrap 3.0.2 -->
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Janys bootstrap -->
        <link href="{{ asset('assets/css/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="{{ asset('assets/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- DATA TABLES -->
        <link href="{{ asset('assets/css/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{ asset('assets/css/admin.css') }}" rel="stylesheet" type="text/css" />
        <!-- Bootstrap wysihtml5 - text editor -->
        <link href="{{ asset('assets/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="assets/js/vendor/html5shiv.js"><\/script>')</script>

            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="{{ route('admin.dashboard') }}" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Administrador
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>{{ Auth::user()->name }} <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="" class="img-circle" alt="{{ Auth::user()->name }}" />
                                    <p>
                                        {{ Auth::user()->email }}
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="admin/sair" class="btn btn-default btn-flat">Sair</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="{{ route('admin.dashboard') }}">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.carousel') }}">
                                <i class="fa fa-picture-o"></i> <span>Banners Home</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.partitions.index') }}">
                                <i class="fa fa-barcode"></i> <span>Divisórias</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-inbox"></i>
                                <span>Acessórios</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ route('admin.accessories.categories.index') }}"><i class="fa fa-angle-double-right"></i> Categorias</a></li>
                                <li><a href="{{ route('admin.accessories.products.index') }}"><i class="fa fa-angle-double-right"></i> Produtos</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="">
                                <i class="fa fa-users"></i> <span>Usuários</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Main content -->
                <section class="content">
                    <div clas="row">
                        @yield('content')
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
        {{ HTML::script('assets/js/vendor/bootstrap.min.js') }}
        {{ HTML::script('assets/js/vendor/jasny-bootstrap.min.js') }}
        {{ HTML::script('assets/js/plugins/datatables/jquery.dataTables.js') }}
        {{ HTML::script('assets/js/plugins/datatables/dataTables.bootstrap.js') }}
        {{ HTML::script('assets/js/admin/app.js') }}
        @yield('customjs')
    </body>
</html>