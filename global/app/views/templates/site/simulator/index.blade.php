@extends('templates.site.layouts.master')

@section('content')
    <div id="simulator">
        <div class="box-image">
            <img id="image-select" class="img-responsive" src="{{ $colors[0]['image'] }}">
            <div class="navigation">
                <ul class="nav-simulator">
                    <li><a class="active" href="#colors">Cores</a></li>
                    <li><a href="#mount">Montagem</a></li>
                </ul>
                <div class="backgroud">
                    <div class="box-border">
                        <div id="colors" class="color-choice">
                            <header>
                                <div class="description">
                                    <div class="color" style="background-color: {{ $colors[0]['hex'] }}"></div>
                                    <p><span class="cod">{{ $colors[0]['cod'] }}</span><br>
                                    <span class="name">{{ $colors[0]['name'] }}</span></p>
                                </div>
                            </header>
                            <section>
                                <?php $i=1; ?>
                                @foreach($colors as $color)
                                    <div class="select-colors{{ $i%5 ? ' space' : '' }}" style="background-color: {{ $color['hex'] }}">
                                        {{ $i==1 ? '<div class="color-active"></div>' : '' }}
                                        {{ Form::hidden('background', $color['hex']); }}
                                        {{ Form::hidden('cod', $color['cod']); }}
                                        {{ Form::hidden('name', $color['name']); }}
                                        {{ Form::hidden('image', $color['image']); }}
                                    </div>
                                    <?php ++$i; ?>
                                @endforeach

                                <div class="clearfix"></div>

                                <div class="special-colors">
                                    <?php $i_special=1; ?>
                                    @foreach($specialColors as $specialColor)
                                        <div class="select-colors{{ $i_special%5 ? ' space' : '' }}" style="background-image: url('{{ $specialColor['background'] }}'); background-position: center center;">
                                            {{ Form::hidden('background', $specialColor['background']); }}
                                            {{ Form::hidden('cod', $specialColor['cod']); }}
                                            {{ Form::hidden('name', $specialColor['name']); }}
                                            {{ Form::hidden('image', $specialColor['image']); }}
                                        </div>
                                        <?php ++$i_special; ?>
                                    @endforeach
                                </div>
                            </section>
                        </div>
                        <div id="mount" class="mount-choice none-choice">
                            <section class="fixacao">
                                <h6>Tipo de Fixação</h6>
                                <div class="col-md-3 col-lg-3 active" data-number="1">
                                    <img class="img-responsive" src="assets/images/simulador/tipo-fixacao/travamento-superior.png" alt="Travamento Superior">
                                    <p>Travamento<br>Superior</p>
                                </div>
                                <div class="col-md-3 col-lg-3" data-number="2">
                                    <img class="img-responsive" src="assets/images/simulador/tipo-fixacao/somente-piso.png" alt="Somente Piso">
                                    <p>Somente<br>Piso</p>
                                </div>
                                <div class="col-md-3 col-lg-3" data-number="3">
                                    <img class="img-responsive" src="assets/images/simulador/tipo-fixacao/piso-teto.png" alt="Piso Teto">
                                    <p>Piso-Teto</p>
                                </div>
                                <div class="col-md-3 col-lg-3" data-number="4">
                                    <img class="img-responsive border-right-none" src="assets/images/simulador/tipo-fixacao/suspenso.png" alt="Suspenso">
                                    <p class="no-left-margin">Suspenso</p>
                                </div>
                            </section>
                            <section class="dobradica">
                                <h6>Dobradiça</h6>
                                <div class="col-md-4 col-lg-4 active" data-number="1">
                                    <img class="img-responsive" src="assets/images/simulador/dobradicas/piano.png" alt="Piano">
                                    <p>Piano</p>
                                </div>
                                <div class="col-md-4 col-lg-4" data-number="2">
                                    <img class="img-responsive" src="assets/images/simulador/dobradicas/vault.png" alt="Vault">
                                    <p>Vault</p>
                                </div>
                                <div class="col-md-4 col-lg-4" data-number="3">
                                    <img class="img-responsive border-right-none" src="assets/images/simulador/dobradicas/sobrepor.png" alt="Sobrepor">
                                    <p class="no-left-margin">Sobrepor</p>
                                </div>
                            </section>
                            <section class="batente">
                                <h6>Batente</h6>
                                <div class="col-md-6 col-lg-6 active" data-number="1">
                                    <img class="img-responsive" src="assets/images/simulador/batente/chanfro.png" alt="Chanfro">
                                    <p>Chanfro</p>
                                </div>
                                <div class="col-md-6 col-lg-6" data-number="2">
                                    <img class="img-responsive border-right-none" src="assets/images/simulador/batente/aluminio.png" alt="Alumínio">
                                    <p class="no-left-margin">Alumínio</p>
                                </div>
                            </section>
                            <section class="parafuso">
                                <h6>Parafuso</h6>
                                <div class="col-md-6 col-lg-6 active" data-number="1">
                                    <img class="img-responsive" src="assets/images/simulador/parafuso/passante.png" alt="Passante">
                                    <p>Passante</p>
                                </div>
                                <div class="col-md-6 col-lg-6" data-number="2">
                                    <img class="img-responsive border-right-none" src="assets/images/simulador/parafuso/nao-passante.png" alt="Não Passante">
                                    <p class="no-left-margin">Não Passante</p>
                                </div>
                            </section>
                            <section class="ferragem">
                                <h6>Tapa Ferragem</h6>
                                <div class="col-md-6 col-lg-6 active" data-number="1">
                                    <img class="img-responsive" src="assets/images/simulador/tapa-ferragem/sim.png" alt="Sim">
                                    <p>Sim</p>
                                </div>
                                <div class="col-md-6 col-lg-6" data-number="2">
                                    <img class="img-responsive border-right-none" src="assets/images/simulador/tapa-ferragem/nao.png" alt="Não">
                                    <p class="no-left-margin">Não</p>
                                </div>
                            </section>
                            <section class="fechadura">
                                <h6>Fechadura</h6>
                                <div class="col-md-6 col-lg-6 active" data-number="1">
                                    <img class="img-responsive" src="assets/images/simulador/fechaduras/retangular.png" alt="Retangular">
                                    <p>Retangular</p>
                                </div>
                                <div class="col-md-6 col-lg-6" data-number="2">
                                    <img class="img-responsive border-right-none" src="assets/images/simulador/fechaduras/circular.png" alt="Circular">
                                    <p class="no-left-margin">Circular</p>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop