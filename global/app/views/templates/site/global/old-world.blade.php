@extends('templates.site.layouts.master')

@section('content')
<div id="world" class="container space-header">
    <img class="img-responsive" src="assets/images/map-mundi-fabricas.png">

    <div class="marker" style="top: 371px; left: 177px;">
        <img src="assets/images/marker-medium.png" alt="Santa Fé Springs, CA 90670">
        <div class="information" style="min-width: 220px;">
            <p><b>Santa Fé Springs, CA 90670</b></p>
        </div>
    </div>

    <div class="marker" style="top: 343px; left: 167px;">
        <img src="assets/images/marker-medium.png" alt="Oakland, CA 94604">
        <div class="information" style="min-width: 180px;">
            <p><b>Oakland, CA 94604</b></p>
        </div>
    </div>

    <div class="marker" style="top: 326px; left: 185px; z-index: 14;">
        <img src="assets/images/marker-medium.png" alt="Sacramento, CA 95828">
        <div class="information" style="min-width: 195px;">
            <p><b>Sacramento, CA 95828</b></p>
        </div>
    </div>

    <div class="marker" style="top: 351px; left: 189px; z-index: 14;">
        <img src="assets/images/marker-medium.png" alt="Santa Clara, CA 95054">
        <div class="information" style="min-width: 185px;">
            <p><b>Santa Clara, CA 95054</b></p>
        </div>
    </div>

    <div class="marker" style="top: 381px; left: 260px;">
        <img src="assets/images/marker-medium.png" alt="Ft. Worth, TX 76117">
        <div class="information" style="min-width: 175px;">
            <p><b>Ft. Worth, TX 76117</b></p>
        </div>
    </div>

    <div class="marker" style="top: 390px; left: 270px;">
        <img src="assets/images/marker-medium.png" alt="Schertz, TX 78154">
        <div class="information" style="min-width: 175px;">
            <p><b>Schertz, TX 78154</b></p>
        </div>
    </div>

    <div class="marker" style="top: 402px; left: 291px;">
        <img src="assets/images/marker-medium.png" alt="Coral Springs, FL 33065">
        <div class="information" style="min-width: 192px;">
            <p><b>Coral Springs, FL 33065</b></p>
        </div>
    </div>

    <div class="marker" style="top: 376px; left: 296px; z-index: 14;">
        <img src="assets/images/marker-medium.png" alt="Fort Myers, FL 33905">
        <div class="information" style="min-width: 192px;">
            <p><b>Fort Myers, FL 33905</b></p>
        </div>
    </div>

    <div class="marker" style="top: 392px; left: 285px;">
        <img src="assets/images/marker-medium.png" alt="Clearwater, FL 33761">
        <div class="information" style="min-width: 192px;">
            <p><b>Clearwater, FL 33761</b></p>
        </div>
    </div>

    <div class="marker" style="top: 332px; left: 330px; z-index: 13;">
        <img src="assets/images/marker-medium.png" alt="Yonkers, NY 10701">
        <div class="information" style="min-width: 170px;">
            <p><b>Yonkers, NY 10701</b></p>
        </div>
    </div>

    <div class="marker" style="top: 310px; left: 290px; z-index: 13;">
        <img src="assets/images/marker-medium.png" alt="Burr Ridge, IL 60527">
        <div class="information" style="min-width: 172px;">
            <p><b>Burr Ridge, IL 60527</b></p>
        </div>
    </div>

    <div class="marker" style="top: 314px; left: 304px; z-index: 13;">
        <img src="assets/images/marker-medium.png" alt="Lyons, IL 60534">
        <div class="information" style="min-width: 143px;">
            <p><b>Lyons, IL 60534</b></p>
        </div>
    </div>

    <div class="marker" style="top: 314px; left: 557px;">
        <img src="assets/images/marker-medium.png" alt="2900 Schoten, Belgium">
        <div class="information" style="min-width: 187px;">
            <p><b>2900 Schoten, Belgium</b></p>
        </div>
    </div>

    <div class="marker" style="top: 383px; left: 197px;">
        <img src="assets/images/marker-medium.png" alt="San Pablo, Tecate, BC 2140 C">
        <div class="information" style="min-width: 228px;">
            <p><b>San Pablo, Tecate, BC 2140 C</b></p>
        </div>
    </div>

    <div class="marker" style="top: 347px; left: 320px; z-index: 13;">
        <img src="assets/images/marker-medium.png" alt="Scranton, PA 18505">
        <div class="information" style="min-width: 168px;">
            <p><b>Scranton, PA 18505</b></p>
        </div>
    </div>

    <div class="marker" style="top: 360px; left: 280px; z-index: 14;">
        <img src="assets/images/marker-medium.png" alt="Toccoa, GA 30577">
        <div class="information" style="min-width: 160px;">
            <p><b>Toccoa, GA 30577</b></p>
        </div>
    </div>

    <div class="marker" style="top: 360px; left: 270px; z-index: 14;">
        <img src="assets/images/marker-medium.png" alt="Eastanollee, GA 30538">
        <div class="information" style="min-width: 185px;">
            <p><b>Eastanollee, GA 30538</b></p>
        </div>
    </div>

    <div class="marker" style="top: 300px; left: 330px; z-index: 12;">
        <img src="assets/images/marker-medium.png" alt="Pickering Ontario LIW 2G8 - Canadá">
        <div class="information" style="min-width: 269px;">
            <p><b>Pickering Ontario LIW 2G8 - Canadá</b></p>
        </div>
    </div>

    <div class="marker" style="top: 580px; right: 170px;">
        <img src="assets/images/marker-medium.png" alt="Australia – Rick Vargiu">
        <div class="information" style="min-width: 183px;">
            <p><b>Australia – Rick Vargiu</b></p>
        </div>
    </div>

</div>
@stop