@extends('templates.site.layouts.master')

@section('content')
<div id="contact" class="space-header">
    <header>
        <h1>Contato</h1>
    </header>
    <section>
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3651.952177748782!2d-46.397470299999995!3d-23.749084699999994!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce12de5e7ef337%3A0x9a0086be0f12a22e!2sR.+Dom+Pedro+I%2C+743+-+Centro%2C+Rio+Grande+da+Serra+-+SP%2C+09450-000!5e0!3m2!1spt-BR!2sbr!4v1415081907905" frameborder="0"></iframe>
        <p class="margin-bottom">
            <b>Endreço: </b>Av. D. Pedro I, 743 - Rio Grande da Serra - SP<br>
            <b>Telefone: </b>+55 11 2548-1555<br>
            <b>Email: </b> <a href="mailto:contato@globaldivisorias.com.br" title="Entre em contato">contato@globaldivisorias.com.br</a>
        </p>
    </section>
</div>
@stop