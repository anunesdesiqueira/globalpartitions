@extends('templates.site.layouts.master')

@section('content')
<div id="world" class="container space-header">
    <div class="box-map">

        <div class="marker" style="top: 215px; left: 141px;">
            <img src="assets/images/marker-medium.png" alt="Santa Fé Springs, CA 90670">
            <div class="information" style="min-width: 216px;">
                <p><b>Santa Fé Springs, CA 90670</b></p>
            </div>
        </div>

        <div class="marker" style="top: 185px; left: 130px;">
            <img src="assets/images/marker-medium.png" alt="Oakland, CA 94604">
            <div class="information" style="min-width: 166px;">
                <p><b>Oakland, CA 94604</b></p>
            </div>
        </div>

        <div class="marker" style="top: 180px; left: 155px; z-index: 14;">
            <img src="assets/images/marker-medium.png" alt="Sacramento, CA 95828">
            <div class="information" style="min-width: 187px;">
                <p><b>Sacramento, CA 95828</b></p>
            </div>
        </div>

        <div class="marker" style="top: 195px; left: 155px; z-index: 14;">
            <img src="assets/images/marker-medium.png" alt="Santa Clara, CA 95054">
            <div class="information" style="min-width: 184px;">
                <p><b>Santa Clara, CA 95054</b></p>
            </div>
        </div>

        <div class="marker" style="top: 224px; left: 220px;">
            <img src="assets/images/marker-medium.png" alt="Ft. Worth, TX 76117">
            <div class="information" style="min-width: 173px;">
                <p><b>Ft. Worth, TX 76117</b></p>
            </div>
        </div>

        <div class="marker" style="top: 231px; left: 226px;">
            <img src="assets/images/marker-medium.png" alt="Schertz, TX 78154">
            <div class="information" style="min-width: 159px;">
                <p><b>Schertz, TX 78154</b></p>
            </div>
        </div>

        <div class="marker" style="top: 243px; left: 254px;">
            <img src="assets/images/marker-medium.png" alt="Coral Springs, FL 33065">
            <div class="information" style="min-width: 192px;">
                <p><b>Coral Springs, FL 33065</b></p>
            </div>
        </div>

        <div class="marker" style="top: 216px; left: 261px; z-index: 14;">
            <img src="assets/images/marker-medium.png" alt="Fort Myers, FL 33905">
            <div class="information" style="min-width: 179px;">
                <p><b>Fort Myers, FL 33905</b></p>
            </div>
        </div>

        <div class="marker" style="top: 233px; left: 248px;">
            <img src="assets/images/marker-medium.png" alt="Clearwater, FL 33761">
            <div class="information" style="min-width: 178px;">
                <p><b>Clearwater, FL 33761</b></p>
            </div>
        </div>

        <div class="marker" style="top: 175px; left: 291px; z-index: 13;">
            <img src="assets/images/marker-medium.png" alt="Yonkers, NY 10701">
            <div class="information" style="min-width: 165px;">
                <p><b>Yonkers, NY 10701</b></p>
            </div>
        </div>

        <div class="marker" style="top: 155px; left: 250px; z-index: 13;">
            <img src="assets/images/marker-medium.png" alt="Burr Ridge, IL 60527">
            <div class="information" style="min-width: 172px;">
                <p><b>Burr Ridge, IL 60527</b></p>
            </div>
        </div>

        <div class="marker" style="top: 156px; left: 270px; z-index: 13;">
            <img src="assets/images/marker-medium.png" alt="Lyons, IL 60534">
            <div class="information" style="min-width: 143px;">
                <p><b>Lyons, IL 60534</b></p>
            </div>
        </div>

        <div class="marker" style="top: 155px; left: 520px;">
            <img src="assets/images/marker-medium.png" alt="2900 Schoten, Belgium">
            <div class="information" style="min-width: 187px;">
                <p><b>2900 Schoten, Belgium</b></p>
            </div>
        </div>

        <div class="marker" style="top: 225px; left: 160px;">
            <img src="assets/images/marker-medium.png" alt="San Pablo, Tecate, BC 2140 C">
            <div class="information" style="min-width: 228px;">
                <p><b>San Pablo, Tecate, BC 2140 C</b></p>
            </div>
        </div>

        <div class="marker" style="top: 189px; left: 283px; z-index: 13;">
            <img src="assets/images/marker-medium.png" alt="Scranton, PA 18505">
            <div class="information" style="min-width: 168px;">
                <p><b>Scranton, PA 18505</b></p>
            </div>
        </div>

        <div class="marker" style="top: 200px; left: 240px; z-index: 14;">
            <img src="assets/images/marker-medium.png" alt="Toccoa, GA 30577">
            <div class="information" style="min-width: 160px;">
                <p><b>Toccoa, GA 30577</b></p>
            </div>
        </div>

        <div class="marker" style="top: 190px; left: 230px; z-index: 14;">
            <img src="assets/images/marker-medium.png" alt="Eastanollee, GA 30538">
            <div class="information" style="min-width: 185px;">
                <p><b>Eastanollee, GA 30538</b></p>
            </div>
        </div>

        <div class="marker" style="top: 143px; left: 295px; z-index: 12;">
            <img src="assets/images/marker-medium.png" alt="Pickering Ontario LIW 2G8 - Canadá">
            <div class="information" style="min-width: 269px;">
                <p><b>Pickering Ontario LIW 2G8 - Canadá</b></p>
            </div>
        </div>

        <div class="marker" style="bottom: 90px; right: 130px;">
            <img src="assets/images/marker-medium.png" alt="Australia – Rick Vargiu">
            <div class="information" style="min-width: 183px;">
                <p><b>Australia – Rick Vargiu</b></p>
            </div>
        </div>

    </div>
</div>
@stop