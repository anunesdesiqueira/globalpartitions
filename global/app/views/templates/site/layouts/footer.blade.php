<footer class="container-fluid footer-main">
    <div class="row">
        <div class="col-md-2 col-lg-2 col-tv720p-2 col-tv1080p-2">
            <a class="footer-brand" href="{{ route('site.home') }}" title="Página Inicial">
                <picture>
                    <source srcset="{{ URL::to('assets/images/logo-footer-medium-global.png') }}" media="(max-width: 1280px)">
                    <source srcset="{{ URL::to('assets/images/logo-footer-default-global.png') }}">
                    <img srcset="{{ URL::to('assets/images/logo-footer-default-global.png') }}" alt="Global Partitions">
                </picture>
            </a>
            <div class="stroke"></div>
            <a class="footer-brand" href="http://asigroup.us" target="_blank" title="ASI">
                <picture>
                    <source srcset="{{ URL::to('assets/images/logo-footer-medium-asi.png') }}" media="(max-width: 1280px)">
                    <source srcset="{{ URL::to('assets/images/logo-footer-default-asi.png') }}">
                    <img srcset="{{ URL::to('assets/images/logo-footer-default-asi.png') }}" alt="ASI">
                </picture>
            </a>
        </div>
        <div class="col-md-10 col-lg-10 col-tv720p-10 col-tv1080p-10">
            <span class="email"><a href="mailto:contato@globaldivisorias.com.br">contato@globaldivisorias.com.br</a></span><div class="partition">|</div><span class="phone">+55 11 2548-1555</span><div class="partition">|</div><address>Av. D. Pedro I, 743 - Rio Grande da Serra - SP</address>
        </div>
    </div>
</footer>