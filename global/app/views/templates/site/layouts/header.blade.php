<header class="header-main-global">
    <div class="navbar navbar-main-global" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-top">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <h1 class="navbar-brand">
                <a href="{{ route('site.home') }}" title="Página Inicial">
                    <picture>
                        <source srcset="{{ URL::to('assets/images/logo-header-medium-global.png') }}" media="(max-width: 1280px)">
                        <source srcset="{{ URL::to('assets/images/logo-header-default-global.png') }}">
                        <img srcset="{{ URL::to('assets/images/logo-header-default-global.png') }}" alt="Global Partitions">
                    </picture>
                </a>
            </h1>
        </div>
        <nav class="collapse navbar-collapse" id="main-nav-top">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Global No Mundo</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ route('site.brazil') }}">Brasil</a></li>
                        <li><a href="{{ route('site.world') }}">Fábricas no Mundo</a></li>
                        <li><a href="{{ route('site.sustainability') }}">Sustentabilidade</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Produtos</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ route('site.partitions.partition', 'divisoria-em-laminado') }}">Divisórias em Laminado</a></li>
                        <li><a href="{{ route('site.partitions.partition', 'divisoria-em-aco-inox') }}">Divisórias em Aço – Inox</a></li>
                        <li><a href="{{ route('site.accessories.categories') }}">Acessórios</a></li>
                    </ul>
                </li>
                <li><a href="{{ URL::to('assets/pdf/brascontainers-modelo3d.pdf') }}" target="_blank">Virtual 360º</a></li>
                <li><a href="{{ route('site.simulator') }}">Simulador</a></li>
                <li><a href="{{ route('site.contact') }}">Contato</a></li>
            </ul>
            <form class="navbar-form pull-right" role="search">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="input-search form-control">
                    <span class="input-group-btn">
                        <button class="btn btn-search" type="button"><i class="fa fa-search rotate"></i></button>
                    </span>
                    </div>
                </div>
            </form>
        </nav>
    </div>
</header>