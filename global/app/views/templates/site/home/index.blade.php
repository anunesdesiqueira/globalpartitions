@extends('templates.site.layouts.master')

@section('content')
    <div id="carousel-global-home" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            @for($i=0; $i < count($allBanners); $i++)
                <li data-target="#carousel-global-home" data-slide-to="{{ $i }}"{{ $i===0 ? ' class="active"' : '' }}></li>
            @endfor
        </ol>

        <div class="banner-description">
            <h3>LÍDER MUNDIAL EM DIVISÓRIAS SANITÁRIAS E ACESSÓRIOS PARA BANHEIROS, AGORA NO BRASIL.<br>
                <span>CONHEÇA AS FÁBRICAS DA GLOBAL PELO MUNDO</span>
                <a class="bt-play" href="{{ route('site.world') }}" title="Fábricas no Mundo">
                    <picture>
                        <source srcset="assets/images/bt-play-medium.png" media="(max-width: 1280px)">
                        <source srcset="assets/images/bt-play-default.png">
                        <img srcset="assets/images/bt-play-default.png" alt="Play">
                    </picture>
                </a>
            </h3>
        </div>

        <div class="carousel-inner">
            @foreach($allBanners as $k => $banner)
                <div class="item{{ $k===0 ? ' active' : '' }}">
                    <img src="{{ $banner->image['default'] }}" alt="{{ $banner->title }}">
                    <div class="local">
                        <h4>
                            {{ $banner->title }}<br>
                            {{ !is_null($banner->description) ? '<span>'.$banner->description.'</span>' : '' }}
                        </h4>
                    </div>
                </div>
            @endforeach
        </div>
        <a class="left carousel-control" href="#carousel-global-home" role="button" data-slide="prev">
            <picture>
                <source srcset="assets/images/carousel-medium-left.png" media="(max-width: 1280px)">
                <source srcset="assets/images/carousel-default-left.png">
                <img srcset="assets/images/carousel-default-left.png" alt="Anterior">
            </picture>
        </a>
        <a class="right carousel-control" href="#carousel-global-home" role="button" data-slide="next">
            <picture>
                <source srcset="assets/images/carousel-medium-right.png" media="(max-width: 1280px)">
                <source srcset="assets/images/carousel-default-right.png">
                <img srcset="assets/images/carousel-default-right.png" alt="Próxima">
            </picture>
        </a>
    </div>
    <div class="clearfix"></div>
    <footer id="home-sections">
        <section class="col-sm-3 col-md-3 col-lg-3 col-tv720p-3 col-tv1080p-3 divisorio-estrutural">
            <a class="white" href="{{ route('site.partitions.partition', 'divisoria-em-laminado') }}" title="Divisórias - Em Laminado Estrutural">
                <h2>Divisórias<br><span>Em Laminado estrutural</span></h2>
            </a>
        </section>
        <section class="col-sm-3 col-md-3 col-lg-3 col-tv720p-3 col-tv1080p-3 divisorio-inox">
            <a class="black" href="{{ route('site.partitions.partition', 'divisoria-em-aco-inox') }}" title="Divisórias - Em Aço - Inox">
                <h2>Divisórias<br><span>Em aço - inox</span></h2>
            </a>
        </section>
        <section class="col-sm-3 col-md-3 col-lg-3 col-tv720p-3 col-tv1080p-3 acessorios">
            <a class="black" href="{{ route('site.accessories.categories') }}" title="Acessórios - Para Banheiro">
                <h2>Acessórios<br><span>Para Banheiro</span></h2>
            </a>
        </section>
        <section class="col-sm-3 col-md-3 col-lg-3 col-tv720p-3 col-tv1080p-3 simulador">
            <a class="white" href="{{ route('site.simulator') }}" title="Simulador - Cores e Acabamentos">
                <h2>Simulador<br><span>Cores e Acabamentos</span></h2>
            </a>
        </section>
    </footer>
@stop