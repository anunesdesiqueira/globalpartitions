@extends('templates.site.layouts.master')

@section('content')
<div id="partitions">
    <img class="img-responsive" src="{{ URL::to('assets/images/divisorias-inox-backgroud.jpg') }}">
    <header>
        <div class="content">
            <div class="prev-header">{{ HTML::image('assets/images/patitions-header-prev.png') }}</div>
            <ol class="breadcrumb">
                <li class="active">Produtos</li>
                <li class="active">Divisórias em Aço Inox</li>
            </ol>
        </div>
    </header>
    <section class="active">
        <div class="details">
            <div class="open">
                <span>Detalhes</span> <a href="#">{{ HTML::image('assets/images/partitions-open.png') }}</a>
            </div>
            <div class="close inactive">
                <a href="#">{{ HTML::image('assets/images/partitions-close.png') }}</a>
            </div>
            <h1>Divisória em Aço Inox</h1>
            <p>
                <b>Anti-vandalismo</b><br>
                Alta resistência a impactos, riscos podem ser removidos com desbaste
            </p>
            <p>
                <b>100% resistente a água</b><br>
                Próprio para ambientes como sanitários e vestiários, alta resistência a ferrugem e corrosão.
            </p>
            <p>
                <b>Higiênico e fácil manutenção</b><br>
                Produto inerte a bactérias e de limpeza e manutenção simples
            </p>
            <a class="btn btn-patitions" href="#" title="Download">Download</a>
            <a class="btn btn-patitions" href="#" title="Ver PDF">Ver PDF</a>
            <a class="btn btn-patitions" href="#" title="Galeria de Fotos">Galeria de Fotos</a>
        </div>
    </section>
</div>
@stop