@extends('templates.site.layouts.master')

@section('content')
<div id="partitions">
    <img class="img-responsive" src="{{ $partition->background['original'] }}">
    <section class="active">
        <div class="details">
            <div class="open inactive">
                <a href="#"><span>Detalhes</span> {{ HTML::image('assets/images/partitions-open.png') }}</a>
            </div>
            <div class="close">
                <a href="#">{{ HTML::image('assets/images/partitions-close.png') }}</a>
            </div>
            <h1>{{ $partition->title }}</h1>
            <p>{{ $partition->content }}</p>
            <a class="btn btn-patitions" href="#" title="Download">Download</a>
            <a class="btn btn-patitions" href="#" title="Ver PDF">Ver PDF</a>
            @if(count($partition->gallery) > 0)
                <a class="btn btn-patitions" href="#" data-toggle="modal" data-target="#carousel-partitions" title="Galeria de Fotos">Galeria de Fotos</a>
            @endif
        </div>
    </section>
</div>

<!-- Modal -->
<div class="modal fade large-modal" id="carousel-partitions" tabindex="-1" role="dialog" aria-labelledby="carousel-partitions-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
                <div id="carousel-partitions-images" class="carousel slide" data-ride="carousel" data-interval="false">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        @for($i=0; $i < count($partition->gallery); $i++)
                            <li data-target="#carousel-partitions" data-slide-to="{{ $i }}"{{ $i===0 ? ' class="active"' : '' }}></li>
                        @endfor
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        @foreach($partition->gallery as $k => $image)
                            <div class="item{{ $k===0 ? ' active' : '' }}">
                                <img src="{{ $image->image['original'] }}">
                            </div>
                        @endforeach
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-partitions-images" role="button" data-slide="prev">
                        <picture>
                            <source srcset="{{ URL::to('assets/images/carousel-medium-left.png') }}" media="(max-width: 1280px)">
                            <source srcset="{{ URL::to('assets/images/carousel-default-left.png') }}">
                            <img srcset="{{ URL::to('assets/images/carousel-default-left.png') }}" alt="Anterior">
                        </picture>
                    </a>
                    <a class="right carousel-control" href="#carousel-partitions-images" role="button" data-slide="next">
                        <picture>
                            <source srcset="{{ URL::to('assets/images/carousel-medium-right.png') }}" media="(max-width: 1280px)">
                            <source srcset="{{ URL::to('assets/images/carousel-default-right.png') }}">
                            <img srcset="{{ URL::to('assets/images/carousel-default-right.png') }}" alt="Próxima">
                        </picture>
                    </a>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>


@stop