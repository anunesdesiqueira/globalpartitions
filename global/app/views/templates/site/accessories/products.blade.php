@extends('templates.site.layouts.master')

@section('content')
    <div id="acessorios" class="space-header">
        <div class="jumbotron">
            <div class="container">
                <h2>Lorem Ipsum</h2>
                <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet<br>consectetur, adipisci velit, sed quia non numquam</p>
            </div>
        </div>
        <ol class="breadcrumb">
            <li><a href="{{ route('site.accessories.categories') }}">categorias de acessórios</a></li>
            <li class="active">{{ $category->name }}</li>
        </ol>
        <section class="detalhe container">
            @foreach($allProducts as $product)
                <article>
                    <aside class="col-md-offset-1 col-md-3 col-lg-3 col-lg-offset-1 col-tv720p-3 col-tv1080p-3">
                        <div class="img-detalhe">
                            <picture>
                                <source srcset="{{ $product->product_images[0]['default'] }}">
                                <img srcset="{{ $product->product_images[0]['default'] }}" alt="{{ $product->product_name }}">
                            </picture>
                            <a class="lupa" href="{{ $product->product_images[0]['original'] }}" title="Ampliar"><i class="fa fa-search"></i></a>
                        </div>
                    </aside>
                    <section class="col-md-7 col-lg-7 col-tv720p-7 col-tv1080p-7">
                        <h1>{{ $product->product_name }}</h1>
                        <span class="cod">{{ $product->product_cod }}</span>
                        <p class="descricao">{{ $product->product_description }}</p>
                        <h6>Dimensões:</h6>
                        <p class="dimensoes">{{ $product->product_dimensions }}</p>
                        @if(count($product->product_downloads) > 0)
                            <h6 class="pull-left">Downloads:</h6>
                            @foreach($product->product_downloads as $k => $download)
                                <a href="#" title="{{ $download->download }}">Download{{ $k+1 }}</a>
                            @endforeach
                        @endif
                    </section>
                    <hr class="col-md-offset-1 col-md-10 col-lg-10 col-lg-offset-1 col-tv720p-10 col-tv1080p-10">
                </article>
            @endforeach
        </section>
        <footer>
            <div class="loader">
                <div class="loading-quarter-circle"></div>
            </div>
        </footer>
    </div>
@stop

@section('customjs')
    {{ HTML::script('assets/js/vendor/magnific-popup.min.js') }}
    <script type="text/javascript">
        $(document).ready(function() {
            $('.lupa').magnificPopup({
                type:'image'
            });
        });
    </script>
@stop