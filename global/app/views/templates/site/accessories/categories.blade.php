@extends('templates.site.layouts.master')

@section('content')
    <div id="acessorios" class="space-header">
        <div class="jumbotron">
            <div class="container">
                <h2>Lorem Ipsum</h2>
                <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet<br>consectetur, adipisci velit, sed quia non numquam</p>
            </div>
        </div>
        <ol class="breadcrumb">
            <li class="active">categorias de acessórios</li>
        </ol>
        <section class="categorias">
            @foreach($allCategories as $category)
                <article>
                    <aside class="col-md-2 col-lg-2 col-tv720p-2 col-tv1080p-2">
                        <a href="{{ route('site.accessories.products', array('permalink' => $category->permalink)) }}" title="{{ $category->name }}">
                            <picture>
                                <source srcset="{{ $category->image['main'] }}" media="(max-width: 1280px)">
                                <source srcset="{{ $category->image['default'] }}">
                                <img srcset="{{ $category->image['default'] }}" alt="{{ $category->name }}">
                            </picture>
                        </a>
                    </aside>
                    <section class="col-md-10 col-lg-10 col-tv720p-10 col-tv1080p-10">
                        <a href="{{ route('site.accessories.products', array('permalink' => $category->permalink)) }}">
                            <h1>{{ $category->name }}</h1>
                            <picture>
                                <source srcset="{{ URL::to('assets/images/acessorios-seta-medium.png') }}" media="(max-width: 1280px)">
                                <source srcset="{{ URL::to('assets/images/acessorios-seta-default.png') }}">
                                <img class="seta" srcset="{{ URL::to('assets/images/acessorios-seta-default.png') }}" alt="Lista de produtos {{ $category->name }}">
                            </picture>
                        </a>
                    </section>
                </article>
            @endforeach
            <div class="clearfix">&nbsp;</div>
        </section>
        <footer>
            <div class="loader">
                <div class="loading-quarter-circle"></div>
            </div>
        </footer>
    </div>
@stop