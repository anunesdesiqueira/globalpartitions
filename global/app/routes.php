<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// ===============================================
// SITE PAGES ====================================
// ===============================================
Route::get('/', array(
    'as'    => 'site.home',
    'uses'  => 'IndexController@index',
));

Route::get('/brasil', array(
    'as'    => 'site.brazil',
    'uses'  => 'IndexController@brazil',
));

Route::get('/fabricas-no-mundo', array(
    'as'    => 'site.world',
    'uses'  => 'IndexController@world',
));

Route::get('/sustentabilidade', array(
    'as'    => 'site.sustainability',
    'uses'  => 'IndexController@sustainability',
));

Route::get('/simulador', array(
    'as'    => 'site.simulator',
    'uses'  => 'SimulatorController@index'
));

Route::get('/produtos/{permalink}', array(
    'as'    => 'site.partitions.partition',
    'uses'  => 'PartitionsController@partition',
));

Route::get('/acessorios', array(
    'as'    => 'site.accessories.categories',
    'uses'  => 'AccessoriesCategoriesController@categories',
));

Route::get('/acessorios/{permalink}', array(
    'as'    => 'site.accessories.products',
    'uses'  => 'AccessoriesProductsController@products',
));

Route::get('/contato', array(
    'as'    => 'site.contact',
    'uses'  => 'IndexController@contact',
));

// ===============================================
// LOGIN SECTION =================================
// ===============================================

Route::group(array('prefix' => 'admin', 'before' => 'guest'), function()
{
    Route::get('/login', array(
        'as'    => 'admin.login',
        'uses'  => 'UserController@index',
    ));

    Route::post('/login', array(
        'as'    => 'admin.login',
        'uses'  => 'UserController@store',
    ));

    Route::get('/esqueci-minha-senha', array(
        'as'    => 'admin.forgot',
        'uses'  => 'UserController@forgot',
    ));

    Route::post('/esqueci-minha-senha', array(
        'as'    => 'admin.forgot',
        'uses'  => 'UserController@postForgot',
    ));
});

// ===============================================
// ADMIN SECTION =================================
// ===============================================

Route::group(array('prefix' => 'admin', 'before' => 'auth'), function()
{
    Route::get('/', array(
        'as'    => 'admin.dashboard',
        'uses'  => 'AdminController@index',
    ));

    Route::get('/sair', function() {

        Auth::logout();
        return Redirect::route('admin.login');
    });

    Route::get('/divisorias', array(
        'as'    => 'admin.partitions.index',
        'uses'  => 'PartitionsController@index',
    ));

    Route::match(array('GET', 'POST'), '/divisorias/nova', array(
        'as'    => 'admin.partitions.create',
        'uses'  => 'PartitionsController@create',
    ));

    Route::match(array('GET', 'POST'), '/divisorias/{id}/editar', array(
        'as'    => 'admin.partitions.edit',
        'uses'  => 'PartitionsController@edit',
    ));

    Route::match(array('GET', 'POST'), '/divisorias/{id}/galeria-de-fotos', array(
        'as'    => 'admin.partitions.gallery',
        'uses'  => 'PartitionsController@gallery',
    ));

    Route::delete('/divisorias/galeria-de-fotos/{id_image}', array(
        'as' => 'admin.partitions.gallery.destroy',
        'uses' => 'PartitionsController@destroy',
    ));

    Route::get('/acessorios/categorias', array(
        'as'    => 'admin.accessories.categories.index',
        'uses'  => 'AccessoriesCategoriesController@index',
    ));

    Route::match(array('GET', 'POST'), '/acessorios/categorias/nova', array(
        'as'    => 'admin.accessories.categories.create',
        'uses'  => 'AccessoriesCategoriesController@create',
    ));

    Route::match(array('GET', 'POST'), '/acessorios/categorias/{id}/editar', array(
        'as'    => 'admin.accessories.categories.edit',
        'uses'  => 'AccessoriesCategoriesController@edit',
    ));

    Route::get('/acessorios/categorias/{id}/excluir', array(
        'as'    => 'admin.accessories.categories.destroy',
        'uses'  => 'AccessoriesCategoriesController@destroy',
    ));

    Route::get('/acessorios/produtos', array(
        'as'    => 'admin.accessories.products.index',
        'uses'  => 'AccessoriesProductsController@index',
    ));

    Route::match(array('GET', 'POST'), '/acessorios/produtos/nova', array(
        'as'    => 'admin.accessories.products.create',
        'uses'  => 'AccessoriesProductsController@create',
    ));

    Route::match(array('GET', 'POST'), '/acessorios/produtos/{id}/editar', array(
        'as'    => 'admin.accessories.products.edit',
        'uses'  => 'AccessoriesProductsController@edit',
    ));

    Route::get('/acessorios/produtos/{id}/excluir', array(
        'as'    => 'admin.accessories.products.destroy',
        'uses'  => 'AccessoriesProductsController@destroy',
    ));

    Route::get('/banners', array(
        'as'    => 'admin.carousel',
        'uses'  => 'CarouselController@index',
    ));

    Route::match(array('GET', 'POST'), '/banners/novo', array(
        'as'    => 'admin.carousel.create',
        'uses'  => 'CarouselController@create',
    ));

    Route::match(array('GET', 'POST'), '/banners/{id}/editar', array(
        'as'    => 'admin.carousel.edit',
        'uses'  => 'CarouselController@edit',
    ));

    Route::get('/banners/{id}/excluir', array(
        'as'    => 'admin.carousel.destroy',
        'uses'  => 'CarouselController@destroy',
    ));

});

// ===============================================
// 404 ===========================================
// ===============================================

App::missing(function($exception)
{
    return Response::view('error', array(), 404);
});


