<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Carousel extends Eloquent {

    use SoftDeletingTrait;

    protected $table = 'carousel';

    protected $fillable = array('title', 'description', 'image', 'order');

    protected $dates = ['deleted_at'];

    /**
     * Find Banner
     * @param $id
     */
    public function findBanner($id)
    {
        return Carousel::find($id);
    }

    /**
     * Show All Banners
     */
    public function allBanners()
    {
        return Carousel::whereNull('deleted_at')
            ->orderBy('order')
            ->orderBy('created_at')
            ->get();
    }

    /**
     * Create Banner
     * @param $data
     * @return bool
     */
    public function createBanner($data)
    {
        $banner = new Carousel;
        $banner->title = $data['title'];
        $banner->description = $data['description'];
        $banner->image = $data['image'];
        $banner->order = $data['order'];

        if($banner->save())
            return true;
        else
            return false;
    }

    /**
     * Update Banner
     * @param $id
     * @param $data
     * @return bool
     */
    public function updateBanner($id, $data)
    {
        $banner = Carousel::find($id);
        $banner->title = $data['title'];
        $banner->description = $data['description'];
        $banner->order = $data['order'];

        // Get nem image
        if(!is_null($data['image']))
            $banner->image = $data['image'];

        if($banner->save())
            return true;
        else
            return false;
    }

    /**
     * Soft Delete Banner
     * @param $id
     * @return mixed
     */
    public function destroyBanner($id)
    {
        return Carousel::find($id)->delete();
    }
}