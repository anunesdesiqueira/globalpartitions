<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class AccessoriesCategories extends Eloquent {

    use SoftDeletingTrait;

    protected $table = 'accessories_categories';

    protected $fillable = array('name', 'order', 'image', 'permalink');

    protected $dates = ['deleted_at'];

    /**
     * Find Accessories Category
     * @param $id
     */
    public function findCategory($id)
    {
        return AccessoriesCategories::find($id);
    }

    public function findCategoryPermalink($permalink)
    {
        return AccessoriesCategories::where('permalink', '=', $permalink)->first();
    }

    /**
     * Show All Accessories Categories
     */
    public function allCategories()
    {
        return AccessoriesCategories::whereNull('deleted_at')
            ->orderBy('order')
            ->orderBy('created_at')
            ->get();
    }

    /**
     * Create Accessories Category
     * @param $data
     * @return bool
     */
    public function createCategory($data)
    {
        $category = new AccessoriesCategories;
        $category->name = $data['name'];
        $category->image = $data['image'];
        $category->order = $data['order'];
        $category->permalink = $data['permalink'];

        if($category->save())
            return true;
        else
            return false;
    }

    /**
     * Update Accessories Category
     * @param $id
     * @param $data
     * @return bool
     */
    public function updateCategory($id, $data)
    {
        $category = AccessoriesCategories::find($id);
        $category->name = $data['name'];

        // Get nem image
        if(!is_null($data['image']))
            $category->image = $data['image'];

        $category->order = $data['order'];
        $category->permalink = $data['permalink'];

        if($category->save())
            return true;
        else
            return false;
    }

    /**
     * Soft Delete Accessories Category
     * @param $id
     * @return mixed
     */
    public function destroyCategory($id)
    {
        return AccessoriesCategories::find($id)->delete();
    }
}