<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class AccessoriesProductsDownloads extends Eloquent {

    use SoftDeletingTrait;

    protected $table = 'accessories_products_downloads';

    protected $fillable = array('product_id', 'download');

    protected $dates = ['deleted_at'];

    /**
     * Find Accessories Product Download
     * @param $id
     */
    public function findProductDownloads($id)
    {
        return AccessoriesProductsDownloads::find($id);
    }

    /**
     * Show All Accessories Products Download
     */
    public function allProductsDownloads($idProduct)
    {
        return AccessoriesProductsDownloads::whereNull('deleted_at')
            ->where('product_id', '=', $idProduct)
            ->orderBy('created_at')
            ->get();
    }

    /**
     * Create Accessories Product Download
     * @param $data
     * @return bool
     */
    public function createProductDownloads($product_id, $file)
    {
        $productDownload = new AccessoriesProductsDownloads;
        $productDownload->product_id = $product_id;
        $productDownload->download = $file;

        if($productDownload->save())
            return true;
        else
            return false;
    }

    /**
     * Update Accessories Product Download
     * @param $id
     * @param $data
     * @return bool
     */
    public function updateProductDownloads($product_id, $file)
    {
        $productDownload = new AccessoriesProductsDownloads;
        $productDownload->product_id = $product_id;
        $productDownload->download = $file;

        if($productDownload->save())
            return true;
        else
            return false;
    }

    /**
     * Soft Delete Accessories Product Image
     * @param $id
     * @return mixed
     */
    public function destroyProduct($id)
    {
        return AccessoriesProductsDownloads::find($id)->delete();
    }
}