<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class PartitionsImages extends Eloquent {

    use SoftDeletingTrait;

    protected $table = 'partitions_images';

    protected $fillable = array('partition_id', 'image');

    protected $dates = ['deleted_at'];

    /**
     * Find Partition Image
     * @param $id
     */
    public function findPartitionImage($id)
    {
        return PartitionsImages::find($id);
    }

    /**
     * Show All Partitions Images
     */
    public function allPartitionsImages($partition_id)
    {
        return PartitionsImages::whereNull('deleted_at')
            ->where('partition_id', '=', $partition_id)
            ->orderBy('created_at')
            ->get();
    }

    /**
     * Create Partition Image
     * @param $id, $image
     * @return bool
     */
    public function createPartitionImage($id, $image)
    {
        $partitionImage = new PartitionsImages;
        $partitionImage->partition_id = $id;
        $partitionImage->image = $image;

        if($partitionImage->save())
            return true;
        else
            return false;
    }

    /**
     * Soft Delete Partition
     * @param $id
     * @return mixed
     */
    public function destroyPartitionImage($id)
    {
        return PartitionsImages::find($id)->delete();
    }
}