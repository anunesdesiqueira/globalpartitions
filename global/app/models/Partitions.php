<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Partitions extends Eloquent {

    use SoftDeletingTrait;

    protected $table = 'partitions';

    protected $fillable = array('title', 'content', 'background', 'permalink', 'order');

    protected $dates = ['deleted_at'];

    /**
     * Find Partition
     * @param $id
     */
    public function findPartition($id)
    {
        return Partitions::find($id);
    }

    /**
     * Find Partition
     * @param $permalink
     */
    public function findPartitionPermalink($permalink)
    {
        return Partitions::where('permalink', '=', $permalink)->first();
    }

    /**
     * Show All Partitions
     */
    public function allPartitions()
    {
        return Partitions::whereNull('deleted_at')
            ->orderBy('order')
            ->orderBy('created_at')
            ->get();
    }

    /**
     * Create Partition
     * @param $data
     * @return bool
     */
    public function createPartition($data)
    {
        $partition = new Partitions;
        $partition->title = $data['title'];
        $partition->content = $data['content'];
        $partition->background = $data['background'];
        $partition->permalink = $data['permalink'];
        $partition->order = $data['order'];

        if($partition->save())
            return true;
        else
            return false;
    }

    /**
     * Update Partition
     * @param $id
     * @param $data
     * @return bool
     */
    public function updatePartition($id, $data)
    {
        $partition = Partitions::find($id);
        $partition->title = $data['title'];
        $partition->content = $data['content'];

        // Get nem image
        if(!is_null($data['background']))
            $partition->background = $data['background'];

        $partition->permalink = $data['permalink'];
        $partition->order = $data['order'];

        if($partition->save())
            return true;
        else
            return false;
    }

    /**
     * Soft Delete Partition
     * @param $id
     * @return mixed
     */
    public function destroyPartition($id)
    {
        return Partitions::find($id)->delete();
    }
}