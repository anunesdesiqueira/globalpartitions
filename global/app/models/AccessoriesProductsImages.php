<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class AccessoriesProductsImages extends Eloquent {

    use SoftDeletingTrait;

    protected $table = 'accessories_products_images';

    protected $fillable = array('product_id', 'image');

    protected $dates = ['deleted_at'];

    /**
     * Find Accessories Product Image
     * @param $id
     */
    public function findProductImages($id)
    {
        return AccessoriesProductsImages::find($id);
    }

    /**
     * Show All Accessories Products Image
     */
    public function allProductsImages($idProduct)
    {
        return AccessoriesProductsImages::whereNull('deleted_at')
            ->where('product_id', '=', $idProduct)
            ->orderBy('created_at')
            ->get();
    }

    /**
     * Create Accessories Product Image
     * @param $data
     * @return bool
     */
    public function createProductImages($product_id, $image)
    {
        $productImage = new AccessoriesProductsImages;
        $productImage->product_id = $product_id;
        $productImage->image = $image;

        if($productImage->save())
            return true;
        else
            return false;
    }

    /**
     * Update Accessories Product Image
     * @param $id
     * @param $data
     * @return bool
     */
    public function updateProduct($product_id, $image)
    {
        $productImage = new AccessoriesProductsImages;
        $productImage->product_id = $product_id;
        $productImage->image = $image;

        if($productImage->save())
            return true;
        else
            return false;
    }

    /**
     * Soft Delete Accessories Product Image
     * @param $id
     * @return mixed
     */
    public function destroyProduct($id)
    {
        return AccessoriesProductsImages::find($id)->delete();
    }
}