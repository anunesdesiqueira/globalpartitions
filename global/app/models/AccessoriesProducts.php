<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class AccessoriesProducts extends Eloquent {

    use SoftDeletingTrait;

    protected $table = 'accessories_products';

    protected $fillable = array('category_id', 'cod', 'name', 'description', 'dimensions', 'order');

    protected $dates = ['deleted_at'];

    /**
     * Find Accessories Product
     * @param $id
     */
    public function findProduct($id)
    {
        return AccessoriesProducts::find($id);
    }

    /**
     * Show All Accessories Products
     */
    public function allProducts($idCategory = null)
    {
        return AccessoriesProducts::join('accessories_categories', 'accessories_categories.id', '=', 'accessories_products.category_id')
            ->where(function ($query) use ($idCategory) {
                if(!is_null($idCategory)) $query->where('category_id', '=', $idCategory);
            })
            ->whereNull('accessories_products.deleted_at')
            ->orderBy('accessories_products.order')
            ->orderBy('accessories_products.created_at')
            ->select(
                'accessories_categories.name as categoy_name',
                'accessories_products.id as product_id',
                'accessories_products.cod as product_cod',
                'accessories_products.name as product_name',
                'accessories_products.order as product_order',
                'accessories_products.description as product_description',
                'accessories_products.dimensions as product_dimensions'
            )->get();
    }

    /**
     * Create Accessories Product
     * @param $data
     * @return bool
     */
    public function createProduct($data)
    {
        $product = new AccessoriesProducts;
        $product->category_id = $data['category_id'];
        $product->cod = $data['cod'];
        $product->name = $data['name'];
        $product->description = $data['description'];
        $product->dimensions = $data['dimensions'];
        $product->order = $data['order'];

        if($product->save())
            return $product->id;
        else
            return false;
    }

    /**
     * Update Accessories Product
     * @param $id
     * @param $data
     * @return bool
     */
    public function updateProduct($id, $data)
    {
        $product = new AccessoriesProducts;
        $product->category_id = $data['category_id'];
        $product->cod = $data['cod'];
        $product->name = $data['name'];
        $product->description = $data['description'];
        $product->dimensions = $data['dimensions'];
        $product->order = $data['order'];

        if($product->save())
            return true;
        else
            return false;
    }

    /**
     * Soft Delete Accessories Product
     * @param $id
     * @return mixed
     */
    public function destroyProduct($id)
    {
        return AccessoriesProducts::find($id)->delete();
    }
}