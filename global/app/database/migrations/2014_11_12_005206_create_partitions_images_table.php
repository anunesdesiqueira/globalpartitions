<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartitionsImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('partitions_images', function($table)
        {
            $table->increments('id');
            $table->integer('partition_id')->unsigned();
            $table->foreign('partition_id')->references('id')->on('partitions');
            $table->text('image');
            $table->softDeletes();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('partitions_images');
	}

}
