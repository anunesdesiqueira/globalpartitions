<?php

class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('user')->delete();
        User::create(array(
            'name'     => 'Adriano Siqueira',
            'email'    => 'adrianodizao@icloud.com',
            'password' => Hash::make('admin'),
        ));
    }

}