<?php

class CarouselController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $carousel = new Carousel();
        $allBanners = $carousel->allBanners();

        return View::make('templates.admin.carousel.index', compact('allBanners'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        if(Request::isMethod('post')) {

            $data = Input::all();

            $validation = \Validator::make($data, array(
                'title'  => 'required|max:155',
                'description' => 'max:155',
                'order' => 'required|numeric',
                'image' => 'required|mimes:jpg,jpeg,png',
            ), array(
                'title.required' => 'Digite o titulo do banner.',
                'title.max'  => 'Titulo excedeu 155 caracteres.',
                'description.max'  => 'Descrição excedeu 155 caracteres.',
                'order.required' => 'Insira uma posição.',
                'order.numeric' => 'Campo ordem é numérico.',
                'image.required' => 'Insira uma imagem para categoria.',
                'image.mimes' => 'Extensões permitidas para imagens são jpg, jpeg, gif e png',
            ));

            if($validation->fails()) {
                return Redirect::route('templates.admin.carousel.create')->withInput()->withErrors($validation);
            } else {
                unset($data['_token']);

                // Upload File
                $config = Config::get('global.images.carousel');
                $data['image'] = $this->uploadImage($data['image'], $this->normalize($data['title']), $config);

                $carousel = new Carousel();
                $result = $carousel->createBanner($data);

                $alert = array();

                if($result)
                    $alert['success'] = '<b>Sucesso!</b> Banner criado com sucesso.';
                else
                    $alert['alert'] = '<b>Whoops!</b> Ocorreu um erro no cadastro do banner.';

                View::share(compact('alert'));
            }
        }

        return View::make('templates.admin.carousel.create');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $carousel = new Carousel();
        $config = Config::get('global.images.carousel');

        if(Request::isMethod('post')) {

            $data = Input::all();

            $validation = \Validator::make($data, array(
                'title'  => 'required|max:155',
                'description' => 'max:155',
                'order' => 'required|numeric',
                'image' => 'mimes:jpg,jpeg,png',
            ), array(
                'title.required' => 'Digite o titulo do banner.',
                'title.max'  => 'Titulo excedeu 155 caracteres.',
                'description.max'  => 'Descrição excedeu 155 caracteres.',
                'order.required' => 'Insira uma posição.',
                'order.numeric' => 'Campo ordem é numérico.',
                'image.required' => 'Insira uma imagem para categoria.',
                'image.mimes' => 'Extensões permitidas para imagens são jpg, jpeg, gif e png',
            ));

            if($validation->fails()) {
                return Redirect::route('templates.admin.carousel.edit')->withInput()->withErrors($validation);
            } else {
                unset($data['_token']);

                if(!is_null($data['image'])) {

                    // Delete Serve old image
                    $banner = $carousel->findBanner($id);
                    $banner->image = $this->pathImages($banner->image, $config);

                    foreach($banner->image as $urlImg) {
                        unlink("public/{$urlImg}");
                    }

                    // Upload File
                    $data['image'] = $this->uploadImage($data['image'], $this->normalize($data['title']), $config);
                }

                $result = $carousel->updateBanner($id, $data);

                $alert = array();

                if($result)
                    $alert['success'] = '<b>Sucesso!</b> Banner atualizado com sucesso.';
                else
                    $alert['alert'] = '<b>Whoops!</b> Ocorreu um erro ao atualizar o banner.';

                View::share(compact('alert'));
            }
        }

        $banner = $carousel->findBanner($id);

        // Get Images
        $banner->image = $this->pathImages($banner->image, $config);

        return View::make('templates.admin.carousel.edit', compact('banner'));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function destroy($id)
    {
        $carousel = new Carousel();
        /*$banner = $carousel->findBanner($id);

        $config = Config::get('global.images.carousel');
        $banner->image = $this->pathImages($banner->image, $config);

        foreach($banner->image as $urlImg) {
            unlink("public/{$urlImg}");
        }*/

        $result = $carousel->destroyBanner($id);

        if($result)
            return Redirect::route('admin.carousel')->with('success_message', '<b>Sucesso!</b> Banner deletado com sucesso.');
        else
            return Redirect::route('admin.carousel')->with('error_message', '<b>Whoops!</b> Ocorreu um ao deletar o banner.');
    }


}
