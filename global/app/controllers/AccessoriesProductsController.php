<?php

class AccessoriesProductsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $products = new AccessoriesProducts();
        $allProducts = $products->allProducts();

        return View::make('templates.admin.accessories.products.index', compact('allProducts'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        if(Request::isMethod('post')) {

            $data = Input::all();

            $validation = \Validator::make($data, array(
                'category_id' => 'required|numeric',
                'cod' => 'required|numeric',
                'name'  => 'required|max:255',
                'description' => 'required',
                'dimensions' => 'required|max:255',
                'order' => 'required|numeric',
                'image' => 'required|mimes:jpg,jpeg,png',
                //'files' => 'mimes:pdf,doc,zip,rar',
            ), array(
                'category_id.required' => 'Categoria é obrigatória',
                'category_id.numeric' => 'Categoria precisa ser um número',
                'name.required' => 'Digite o nome da categoria.',
                'name.max'  => 'Nome da categoria excedeu 255 caracteres.',
                'description.required' => 'Insira a descrição.',
                'dimensions.required' => 'Insira a dimensões do produto',
                'dimensions.max' => 'Dimensões excedeu 255 caracteres.',
                'order.required' => 'Insira uma posição.',
                'order.numeric' => 'Campo ordem é numérico.',
                'image.required' => 'Insira uma imagem para categoria.',
                'image.mimes' => 'Extensões permitidas para imagens são jpg, jpeg, gif e png',
                //'files.mimes' => 'Extensões permitidas para downloads são pdf, doc, zip e rar',
            ));

            if($validation->fails()) {
                return Redirect::route('admin.accessories.products.create')->withInput()->withErrors($validation);
            } else {
                unset($data['_token']);

                $products = new AccessoriesProducts();
                $result = $products->createProduct($data);

                $alert = array();

                if($result) {

                    // Upload Image
                    $config = Config::get('global.images.accessories.product');
                    $data['image'] = $this->uploadImage($data['image'], $this->normalize($data['name']), $config);

                    $imagesProducts = new AccessoriesProductsImages();
                    $resultImage = $imagesProducts->createProductImages($result, $data['image']);

                    if($resultImage) {

                        // Download
                        $filesDownloads = new AccessoriesProductsDownloads();

                        $config = Config::get('global.downlaods.products');
                        foreach($data['files'] as $k => $file) {
                            $nameFile = $this->uploadDownload($file, $this->normalize($data['name']), $config);
                            $resultImage = $filesDownloads->createProductDownloads($result, $nameFile);

                            if(!$resultImage)
                                $alert['alert'] = '<b>Whoops!</b> Ocorreu um erro no cadastro do produto.';
                        }

                        $alert['success'] = '<b>Sucesso!</b> Produto criada com sucesso.';
                    } else {
                        $alert['alert'] = '<b>Whoops!</b> Ocorreu um erro no cadastro do produto.';
                    }
                } else {
                    $alert['alert'] = '<b>Whoops!</b> Ocorreu um erro no cadastro do produto.';
                }

                View::share(compact('alert'));
            }
        }

        $listsCategories = DB::table('accessories_categories')->lists('name', 'id');
        return View::make('templates.admin.accessories.products.create', compact('listsCategories'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $products = new AccessoriesProducts();
        $config = Config::get('global.images.accessories.product');

        if(Request::isMethod('post')) {

            $data = Input::all();

            $validation = \Validator::make($data, array(
                'category_id' => 'required|numeric',
                'cod' => 'required|numeric',
                'name'  => 'required|max:255',
                'description' => 'required',
                'dimensions' => 'required|max:255',
                'order' => 'required|numeric',
                'image' => 'mimes:jpg,jpeg,png',
            ), array(
                'category_id.required' => 'Categoria é obrigatória',
                'category_id.numeric' => 'Categoria precisa ser um número',
                'name.required' => 'Digite o nome da categoria.',
                'name.max'  => 'Nome da categoria excedeu 255 caracteres.',
                'description.required' => 'Insira a descrição.',
                'dimensions.required' => 'Insira a dimensões do produto',
                'dimensions.max' => 'Dimensões excedeu 255 caracteres.',
                'order.required' => 'Insira uma posição.',
                'order.numeric' => 'Campo ordem é numérico.',
                'image.mimes' => 'Extensões permitidas para imagens são jpg, jpeg, gif e png',
            ));

            if($validation->fails()) {
                return Redirect::route('admin.accessories.products.edit')->withInput()->withErrors($validation);
            } else {
                unset($data['_token']);

                if(!is_null($data['image'])) {

                    // Delete Serve old image
                    $product = $products->findProduct($id);
                    $product->image = $this->pathImages($product->image, $config);

                    foreach($product->image as $urlImg) {
                        unlink("public/{$urlImg}");
                    }

                    // Upload File
                    $data['image'] = $this->uploadImage($data['image'], $this->normalize($data['name']), $config);

                    $image_products = new AccessoriesProductsImages();
                    $result_image = $image_products->createProductImages($id, $data['image']);

                    if(!$result_image)
                        $alert['alert'] = '<b>Whoops!</b> Ocorreu um erro ao atualizar o produto.';
                }

                $result = $products->updateProduct($id, $data);

                $alert = array();

                if($result)
                    $alert['success'] = '<b>Sucesso!</b> Categoria atualizada com sucesso.';
                else
                    $alert['alert'] = '<b>Whoops!</b> Ocorreu um erro ao atualizar a categoria.';

                View::share(compact('alert'));
            }
        }

        $product = $products->findProduct($id);

        // Get Images
        //$product->image = $this->pathImages($product->image, $config);

        return View::make('templates.admin.accessories.products.edit', compact('product'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function destroy($id)
    {
        $products = new AccessoriesProducts();
        /*$product = $products->findProduct($id);

        $config = Config::get('global.images.accessories.product');
        $product->image = $this->pathImages($product->image, $config);

        foreach($product->image as $urlImg) {
            unlink("public/{$urlImg}");
        }*/

        $result = $products->destroyProduct($id);

        if($result)
            return Redirect::route('admin.accessories.products.index')->with('success_message', '<b>Sucesso!</b> Produto deletado com sucesso.');
        else
            return Redirect::route('admin.accessories.products.index')->with('error_message', '<b>Whoops!</b> Ocorreu um ao deletar o produto.');
    }

    public function products($permalink) {
        $categories = new AccessoriesCategories();
        $products = new AccessoriesProducts();
        $images = new AccessoriesProductsImages();
        $downloads = new AccessoriesProductsDownloads();

        $configImages = Config::get('global.images.accessories.product');

        /* Get Category */
        $category = $categories->findCategoryPermalink($permalink);

        /* Get All Products */
        $allProducts = $products->allProducts($category->id);

        /* Get Images and Downloads */
        foreach($allProducts as $k => $product) {

            $allProducts[$k]->product_downloads = $downloads->allProductsDownloads($product->product_id);
            $allProducts[$k]->product_images = $images->allProductsImages($product->product_id);

            foreach($allProducts[$k]->product_images as $keyImage => $image) {
                $allProducts[$k]->product_images[$keyImage] = $this->pathImages($image->image, $configImages);
            }
        }

        return View::make('templates.site.accessories.products', compact('category', 'allProducts'));
    }
}
