<?php

class SimulatorController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $colors = Config::get('simulator.colors');
        $specialColors = Config::get('simulator.specialColors');

        return View::Make('templates.site.simulator.index', compact('colors', 'specialColors'));
	}



}
