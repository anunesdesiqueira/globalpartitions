<?php

class PartitionsController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $partitions = new Partitions();
        $allPartitions = $partitions->allPartitions();

        return View::make('templates.admin.partitions.index', compact('allPartitions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if(Request::isMethod('post')) {

            $data = Input::all();

            $validation = \Validator::make($data, array(
                'title'  => 'required|max:255',
                'content' => 'required',
                'background' => 'required|mimes:jpg,jpeg,png',
                'permalink' => 'required|max:255',
                'order' => 'required|numeric',
            ), array(
                'title.required' => 'Digite o Titulo da divisória.',
                'title.max'  => 'Titulo da divisória excedeu 255 caracteres.',
                'content.required' => 'Insira a descrição da divisória',
                'order.required' => 'Insira uma posição.',
                'order.numeric' => 'Campo ordem é numérico.',
                'background.required' => 'Insira uma imagem para que seja usada de fundo para divisória.',
                'background.mimes' => 'Extensões permitidas para fundo são jpg, jpeg, gif e png',
            ));

            if($validation->fails()) {
                return Redirect::route('admin.partitions.create')->withInput()->withErrors($validation);
            } else {
                unset($data['_token']);

                // Upload Image
                $config = Config::get('global.images.partitions.background');
                $data['background'] = $this->uploadImage($data['background'], $this->normalize($data['title']), $config);

                $partitions = new Partitions();
                $result = $partitions->createPartition($data);

                $alert = array();

                if($result) {
                    $alert['success'] = '<b>Sucesso!</b> Divisória criada com sucesso.';
                } else {
                    $alert['alert'] = '<b>Whoops!</b> Ocorreu um erro no cadastro da divisória.';
                }

                View::share(compact('alert'));
            }
        }

        return View::make('templates.admin.partitions.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $partitions = new Partitions();
        $config = Config::get('global.images.partitions.background');

        if(Request::isMethod('post')) {

            $data = Input::all();

            $validation = \Validator::make($data, array(
                'title'  => 'required|max:255',
                'content' => 'required',
                'background' => 'mimes:jpg,jpeg,png',
                'permalink' => 'required|max:255',
                'order' => 'required|numeric',
            ), array(
                'title.required' => 'Digite o Titulo da divisória.',
                'title.max'  => 'Titulo da divisória excedeu 255 caracteres.',
                'content.required' => 'Insira a descrição da divisória',
                'order.required' => 'Insira uma posição.',
                'order.numeric' => 'Campo ordem é numérico.',
                'background.mimes' => 'Extensões permitidas para fundo são jpg, jpeg, gif e png',
            ));

            if($validation->fails()) {
                return Redirect::route('admin.partitions.edit')->withInput()->withErrors($validation);
            } else {
                unset($data['_token']);

                if(!is_null($data['background'])) {

                    // Upload File
                    $data['background'] = $this->uploadImage($data['background'], $data['permalink'], $config);
                }

                $result = $partitions->updatePartition($id, $data);

                $alert = array();

                if($result)
                    $alert['success'] = '<b>Sucesso!</b> Divisória atualizada com sucesso.';
                else
                    $alert['alert'] = '<b>Whoops!</b> Ocorreu um erro ao atualizar a divisória.';

                View::share(compact('alert'));
            }
        }

        $partition = $partitions->findPartition($id);

        // Get Images
        $partition->background = $this->pathImages($partition->background, $config);

        return View::make('templates.admin.partitions.edit', compact('partition'));
    }

    public function gallery($id)
    {
        $partitions = new Partitions();
        $partition = $partitions->findPartition($id);

        $partitionsImages = new PartitionsImages();

        $config = Config::get('global.images.partitions.gallery');

        if(Request::isMethod('post')) {

            $data = Input::all();

            if(count($data['image']) > 0) {
                foreach($data['image'] as $image) {
                    $nameImage = $this->uploadImage($image, str_random(12), $config);
                    $result = $partitionsImages->createPartitionImage($id, $nameImage);

                    $alert = array();

                    if(!$result) {
                        $alert['alert'] = '<b>Whoops!</b> Ocorreu um erro ao cadastrar as imagens na galeria.';
                    }
                }

                $alert['success'] = '<b>Sucesso!</b> Galeria atualizada com sucesso.';
            }
        }

        $partition->gallery = $partitionsImages->allPartitionsImages($id);

        foreach($partition->gallery as $k => $img) {
            $partition->gallery[$k]->image = $this->pathImages($img->image, $config);
        }

        return View::make('templates.admin.partitions.gallery', compact('partition'));
    }

    public function destroy($id)
    {
        $partitions = new PartitionsImages();
        $result = $partitions->destroyPartitionImage($id);

        if($result)
            echo 1;
        else
            echo 0;

    }

    public function partition($permalink) {

        $partitions = new Partitions();
        $partitionsImages = new PartitionsImages();
        $configBackground = Config::get('global.images.partitions.background');
        $configGallery = Config::get('global.images.partitions.gallery');

        $partition = $partitions->findPartitionPermalink($permalink);

        // Get Images
        $partition->background = $this->pathImages($partition->background, $configBackground);

        // Get Gallery
        $partition->gallery = $partitionsImages->allPartitionsImages($partition->id);

        foreach($partition->gallery as $k => $img) {
            $partition->gallery[$k]->image = $this->pathImages($img->image, $configGallery);
        }

        return View::make('templates.site.partitions.partition', compact('partition'));
    }
}
