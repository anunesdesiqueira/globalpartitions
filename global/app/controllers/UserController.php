<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return View::make('templates.admin.user.login');
	}

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $credentials = array(
            'email'     => Input::get('email'),
            'password'  => Input::get('password'),
            'deleted_at' => null,
        );

        $remember = Input::has('remember_me') AND "1" === Input::get('remember_me') ? true : false;

        if(Auth::attempt($credentials, $remember)) {
            return Redirect::route('admin.dashboard');
        }

        return Redirect::route('admin.login')->withInput()->with('error_message', '<b>Whoops!</b> Por favor, verifique seu usuário e senha.');
    }

    /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        //return View::make('user.create')->with('create', create);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
}
