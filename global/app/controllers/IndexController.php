<?php

class IndexController extends \BaseController {

    public function index()
	{
        $carousel = new Carousel();
        $allBanners = $carousel->allBanners();
        $config = Config::get('global.images.carousel');

        foreach($allBanners as $k => $banner) {
            $allBanners[$k]->image = $this->pathImages($banner->image, $config);
        }

        return View::make('templates.site.home.index', compact('allBanners'));
	}

    public function brazil()
    {
        return View::make('templates.site.global.brazil');
    }

    public function world()
    {
        return View::make('templates.site.global.world');
    }

    public function sustainability()
    {
        return View::make('templates.site.global.sustainability');
    }

    public function contact()
    {
        return View::make('templates.site.global.contact');
    }
}
