<?php

class AccessoriesCategoriesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $categories = new AccessoriesCategories();
        $allCategories = $categories->allCategories();

        return View::make('templates.admin.accessories.categories.index', compact('allCategories'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        if(Request::isMethod('post')) {

            $data = Input::all();

            $validation = \Validator::make($data, array(
                'name'  => 'required|max:255',
                'image' => 'required|mimes:jpg,jpeg,png',
                'order' => 'required|numeric',
                'permalink' => 'required|max:255',
            ), array(
                'name.required' => 'Digite o nome da categoria.',
                'name.max'  => 'Nome da categoria excedeu 255 caracteres.',
                'permalink.required' => 'Insira um link para categoria.',
                'permalink.max' => 'Link excedeu 255 caracteres.',
                'order.required' => 'Insira uma posição.',
                'order.numeric' => 'Campo ordem é numérico.',
                'image.required' => 'Insira uma imagem para categoria.',
                'image.mimes' => 'Extensões permitidas para imagens são jpg, jpeg, gif e png',
            ));

            if($validation->fails()) {
                return Redirect::route('templates.admin.accessories.categories.create')->withInput()->withErrors($validation);
            } else {
                unset($data['_token']);

                // Upload File
                $config = Config::get('global.images.accessories.category');
                $data['image'] = $this->uploadImage($data['image'], $data['permalink'], $config);

                $categories = new AccessoriesCategories();
                $result = $categories->createCategory($data);

                $alert = array();

                if($result)
                    $alert['success'] = '<b>Sucesso!</b> Categoria criada com sucesso.';
                else
                    $alert['alert'] = '<b>Whoops!</b> Ocorreu um erro no cadastro da categoria.';

                View::share(compact('alert'));
            }
        }

        return View::make('templates.admin.accessories.categories.create');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $categories = new AccessoriesCategories();
        $config = Config::get('global.images.accessories.category');

        if(Request::isMethod('post')) {

            $data = Input::all();

            $validation = \Validator::make($data, array(
                'name'  => 'required|max:255',
                'image' => 'mimes:jpg,jpeg,png',
                'order' => 'required|numeric',
                'permalink' => 'required|max:255',
            ), array(
                'name.required' => 'Digite o nome da categoria.',
                'name.max'  => 'Nome da categoria excedeu 255 caracteres.',
                'permalink.required' => 'Insira um link para categoria.',
                'permalink.max' => 'Link excedeu 255 caracteres.',
                'order.required' => 'Insira uma posição.',
                'order.numeric' => 'Campo ordem é numérico.',
                'image.mimes' => 'Extensões permitidas para imagens são jpg, jpeg, gif e png',
            ));

            if($validation->fails()) {
                return Redirect::route('templates.admin.accessories.categories.edit')->withInput()->withErrors($validation);
            } else {
                unset($data['_token']);

                if(!is_null($data['image'])) {

                    // Delete Serve old image
                    /*$category = $categories->findCategory($id);
                    $category->image = $this->pathImages($category->image, $config);

                    foreach($category->image as $urlImg) {
                        unlink("public/{$urlImg}");
                    }*/

                    // Upload File
                    $data['image'] = $this->uploadImage($data['image'], $data['permalink'], $config);
                }

                $result = $categories->updateCategory($id, $data);

                $alert = array();

                if($result)
                    $alert['success'] = '<b>Sucesso!</b> Categoria atualizada com sucesso.';
                else
                    $alert['alert'] = '<b>Whoops!</b> Ocorreu um erro ao atualizar a categoria.';

                View::share(compact('alert'));
            }
        }

        $category = $categories->findCategory($id);

        // Get Images
        $category->image = $this->pathImages($category->image, $config);

        return View::make('templates.admin.accessories.categories.edit', compact('category'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $categories = new AccessoriesCategories();
        /*
        // Delete Serve old image
        $category = $categories->findCategory($id);

        $config = Config::get('global.images.accessories.category');
        $category->image = $this->pathImages($category->image, $config);

        foreach($category->image as $urlImg) {
            unlink("public/{$urlImg}");
        }
        */
        $result = $categories->destroyCategory($id);

        if($result)
            return Redirect::route('templates.admin.accessories.categories.index')->with('success_message', '<b>Sucesso!</b> Categoria deletada com sucesso.');
        else
            return Redirect::route('templates.admin.accessories.categories.index')->with('error_message', '<b>Whoops!</b> Ocorreu um ao deletar categoria.');
	}

    public function categories()
    {
        $categories = new AccessoriesCategories();
        $allCategories = $categories->allCategories();
        $config = Config::get('global.images.accessories.category');

        foreach($allCategories as $k => $category) {
            $allCategories[$k]->image = $this->pathImages($category->image, $config);
        }

        return View::make('templates.site.accessories.categories', compact('allCategories'));
    }
}
