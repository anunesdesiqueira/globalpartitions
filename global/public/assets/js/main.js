$(function() {
    $('.marker').hover(
        function() {
            $(this).find('.information').fadeIn( 500 );
        },
        function() {
            $(this).find('.information').fadeOut( 100 );
        }
    );

    $('#simulator .nav-simulator li a').on('click', function(e) {
        e.preventDefault();

        var $link = $(this);
        var $choice = $link.attr('href');

        if($link.hasClass('active')) return false;

        $('#simulator .nav-simulator li a').removeClass('active');
        $('#colors, #mount').addClass('none-choice');
        $($choice).removeClass('none-choice');

        /* Load Choice */
        if($choice === '#colors')
            loadColors();
        else
            loadMount();

    });

    $('#simulator .select-colors').on('click', function(e) {
        e.preventDefault();

        var $this   = $(this);
        var $background = $this.find('input[name="background"]').val();
        var $cod    = $this.find('input[name="cod"]').val();
        var $name   = $this.find('input[name="name"]').val();
        var $image  = $this.find('input[name="image"]').val();
        var $imageSelect   = $('#image-select');

        var $descriptionColor  = $('#simulator .color');
        var $descriptionCod    = $('#simulator .cod');
        var $descriptionName   = $('#simulator .name');

        $('#simulator .color-active').remove();
        $(this).append('<div class="color-active"></div>');

        $descriptionColor.removeAttr('style');

        if(null === $background.match(/(?!\"|\')[a-z0-9\-\.\/]+\.(?:jpe?g|png|gif)(?!\"|\')/g))
            $descriptionColor.css('background-color', $background);
        else
            $descriptionColor.css('background-image', 'url('+ $background +')');

        $descriptionCod.html($cod);
        $descriptionName.html($name);
        $imageSelect.attr('src', $image);
    });

    $('#simulator .mount-choice section div').on('click', function(e) {
        e.preventDefault();

        var $bt = $(this);
        var $parent = $(this).parent();
        var $imageSelect   = $('#image-select');

        $parent.find('div').removeClass('active');
        $bt.addClass('active');

        var numbers = [];
        var i = 0;
        var $actives = $('#simulator .mount-choice section .active');

        $.each($actives, function(index) {
            numbers[i] = $($actives[index]).attr('data-number');
            ++i;
        });

        $imageSelect.attr('src', 'uploads/simulador/montagem/'+ numbers.join("-") +'.jpg');
    });

    $("#partitions .open a").on("click", function(e) {
        e.preventDefault();

        $('#partitions .open').addClass('inactive');
        $('#partitions .close').removeClass('inactive');

        openSidepage();
    });

    $("#partitions .close a").on("click", function(e) {
        e.preventDefault();

        $('#partitions .close').addClass('inactive');
        $('#partitions .open').removeClass('inactive');

        closeSidepage();
    });


});

function loadColors() {
    var $selectColor = $('#colors .color-active');
    var $image = $selectColor.parent().find('input[name="image"]').val();
    var $imageSelect   = $('#image-select');

    $imageSelect.attr('src', $image);
}

function loadMount() {
    var $imageSelect   = $('#image-select');

    var numbers = [];
    var i = 0;
    var $actives = $('#simulator .mount-choice section .active');

    $.each($actives, function(index) {
        numbers[i] = $($actives[index]).attr('data-number');
        ++i;
    });

    $imageSelect.attr('src', 'uploads/simulador/montagem/'+ numbers.join("-") +'.jpg');
}

function openSidepage() {
    var $details = $('#partitions .details');
    autoHeightAnimate($details, 500);
}

function closeSidepage() {
    $('#partitions .details').stop().animate({
        height: '50px'
    }, 300);
}

function autoHeightAnimate(element, time){
    var curHeight = element.height(), // Get Default Height
        autoHeight = element.css('height', 'auto').height(); // Get Auto Height
    element.height(curHeight); // Reset to Default Height
    element.stop().animate({ height: autoHeight + 25 }, parseInt(time)); // Animate to Auto Height
}